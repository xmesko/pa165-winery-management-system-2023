INSERT INTO barrel (id, acidity, alcohol_level, fill_date, sugar_level, volume) VALUES
(-1, 0.0, 0.0, '2019-01-01', 0.0, 3),
(-2, 10.0, 5.0, '2013-05-09', 2, 2);

INSERT INTO ingredient (id, amount, name, price) VALUES
(-1, 20, 'sugar', 2),
(-2, 30, 'water', 0.2);

INSERT INTO recipe (id, aging_time, "grape-wine_ratio", technical_process) VALUES
(-1, 2, 0.5, 'blending'),
(-2, 3, 0.3, 'fermentation');

INSERT INTO recipe_other_ingredients (recipe_id, other_ingredients_id) VALUES
(-1, 1),
(-2, 2);
