INSERT INTO stock_wine (id, bottles_available, bottles_sold, price, profit) VALUES
(-1, 20, 15, 24, 1200),
(-2, 10, 5, 30, 1500),
(-3, 30, 20, 20, 1000);

INSERT INTO wine (id, alcohol_content, color, description, sweetness, variety, year) VALUES
(-1, 12.5, 0, 'A dry red wine', 1, 'Cabernet Sauvignon', 2015),
(-2, 13.5, 1, 'A dry white wine', 2, 'Chardonnay', 2016),
(-3, 11.5, 0, 'A sweet red wine', 1, 'Merlot', 2017);

INSERT INTO wines_order (id, address_city, address_country, address_street, telephone_number, account_id, address_zip_code) VALUES
(-1, 'New York', 'USA', 'Broadway', '123456789', 1, '10001'),
(-2, 'New York', 'USA', 'Broadway', '123456789', 2, '10001'),
(-3, 'New York', 'USA', 'Broadway', '123456789', 3, '10001');

INSERT INTO wines_order_mappings (wines_order_id, amount, wine_id) VALUES
(-1, 2, 1),
(-1, 3, 2),
(-2, 1, 2),
(-3, 2, 3);
