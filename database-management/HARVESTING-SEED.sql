INSERT INTO field (id, harvest_date, harvest_size, variety) VALUES
(-1, '2019-10-01', 100, 'Pinot Noir'),
(-2, '2023-12-01', 200, 'Pinot Air'),
(-3, '2023-12-05', 300, 'Chardonnay');

INSERT INTO raw_grapes (id, amount, variety, quality) VALUES
(-1, 100, 'Pinot Noir', 'Average'),
(-2, 200, 'Pinot Air', 'Surprisingly salty'),
(-3, 300, 'Chardonnay', 'Very good');
