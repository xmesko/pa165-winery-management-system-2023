#!/bin/bash

# container name as the first argument (the name is the same as database name)
containerName=$1

docker exec -i "$containerName" psql -U postgres -d "$containerName" < "$2"

echo "Seeding of $containerName was successful"
