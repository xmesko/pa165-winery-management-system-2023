#!/bin/bash

docker exec -i inventory-service-db bash < ./database-management/clean.sh
docker exec -i harvesting-service-db bash < ./database-management/clean.sh
docker exec -i production-service-db bash < ./database-management/clean.sh
