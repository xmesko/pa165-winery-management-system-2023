#!/bin/bash

database_name=$(psql -U postgres -At -c "SELECT datname FROM pg_database WHERE datname LIKE '%-service-db'" | xargs)
echo $database_name

psql -U postgres -d "$database_name" <<EOF
DO
\$do\$
DECLARE
    tbl_name text;
BEGIN
    FOR tbl_name IN (SELECT tablename FROM pg_tables WHERE schemaname = 'public') LOOP
        EXECUTE 'TRUNCATE TABLE ' || quote_ident(tbl_name) || ' CASCADE;';
    END LOOP;
END
\$do\$;
EOF

echo "Cleaning $database_name was successful"
