#!/bin/bash

docker exec -i inventory-service-db psql -U postgres -d inventory-service-db < ./database-management/INVENTORY-SEED.sql
docker exec -i harvesting-service-db psql -U postgres -d harvesting-service-db < ./database-management/HARVESTING-SEED.sql
docker exec -i production-service-db psql -U postgres -d production-service-db < ./database-management/PRODUCTION-SEED.sql

echo "Seeding was successful"
