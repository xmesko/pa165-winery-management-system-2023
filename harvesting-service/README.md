# Harvesting service
The Harvesting service provides functionality to manage the fields and grape harvesting.
Each field has a certain size, specific grape vines, and an optimal harvesting date.
Harvest produces raw grapes, these have variety, quantity and quality.
The following section describes high-level functionality and necessary information to run the service.
Further info can be found in the openapi specification.

<hr />

## Responsibilities

**From manager perspective:**
1. Add new field with a specific grape vine variety
2. Edit expected harvest size
3. Edit optimal harvest date
4. Harvest
5. Annotate quality and of harvested raw grapes 

<hr />

## How to run
1. `mvn clean install`
2. `mvn spring-boot:run`


