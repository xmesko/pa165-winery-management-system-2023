package cz.fi.muni.data.repository;

import cz.fi.muni.data.model.RawGrapes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface RawGrapesRepository extends JpaRepository<RawGrapes, Long> {
}
