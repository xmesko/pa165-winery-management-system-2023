package cz.fi.muni.data.repository;

import cz.fi.muni.data.model.Field;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface FieldRepository extends JpaRepository<Field, Long> {
}
