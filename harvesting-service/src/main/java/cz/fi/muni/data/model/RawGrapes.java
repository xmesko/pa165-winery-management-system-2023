package cz.fi.muni.data.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;

import java.io.Serializable;
import java.util.Objects;


@Entity
@Table(name = "raw_grapes", schema = "public", catalog = "harvesting")
public class RawGrapes implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id",columnDefinition = "INT", nullable = false)
    private Long id;

    @NotNull
    @Column(name = "amount",columnDefinition = "INT")
    private int amount;

    @NotNull
    @Column(name = "variety",columnDefinition = "VARCHAR(255)")
    private String variety;

    @NotNull
    @Column(name = "quality",columnDefinition = "VARCHAR(255)")
    private String quality;

    public RawGrapes() {
    }

    public RawGrapes(int amount, String variety, String quality) {
        this.amount = amount;
        this.variety = variety;
        this.quality = quality;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getId() {
        return id;
    }
    public void setAmount(int amount) {
        this.amount = amount;
    }
    public int getAmount() {
        return amount;
    }
    public void setVariety(String variety) {
        this.variety = variety;
    }
    public String getVariety() {
        return variety;
    }
    public void setQuality(String quality) {
        this.quality = quality;
    }
    public String getQuality() {
        return quality;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RawGrapes r)) return false;
        return amount == r.amount &&
                Objects.equals(id, r.id) &&
                Objects.equals(variety, r.variety) &&
                Objects.equals(quality, r.quality);
    }
    @Override
    public int hashCode() {
        return Objects.hash(id, amount, variety, quality);
    }

    @Override
    public String toString() {
        return "RawGrapes{" +
                "id=" + id +
                ", amount=" + amount +
                ", variety='" + variety + '\'' +
                ", quality='" + quality + '\'' +
                '}';
    }
}
