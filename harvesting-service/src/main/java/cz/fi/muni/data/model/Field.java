package cz.fi.muni.data.model;


import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "field", schema = "public", catalog = "harvesting")
@SequenceGenerator(name = "field_id_seq", sequenceName = "field_id_seq", allocationSize = 1)

public class Field implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "field_id_seq")
    private Long id;

    @NotNull
    @Column(name = "harvest_size",columnDefinition = "INT")
    private int harvestSize;

    @NotNull
    @Column(name = "harvest_date",columnDefinition = "DATE")
    private Date harvestDate;

    @NotNull
    @Column(name = "variety",columnDefinition = "VARCHAR(255)")
    private String variety;

    public Field() {
    }

    public Field(int harvestSize, Date harvestDate, String variety) {
        this.harvestSize = harvestSize;
        this.harvestDate = harvestDate;
        this.variety = variety;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getId() {
        return id;
    }
    public void setHarvestSize(int harvestSize) {
        this.harvestSize = harvestSize;
    }
    public int getHarvestSize() {
        return harvestSize;
    }
    public void setHarvestDate(Date harvestDate) {
        this.harvestDate = harvestDate;
    }
    public Date getHarvestDate() {
        return harvestDate;
    }
    public void setVariety(String variety) {
        this.variety = variety;
    }
    public String getVariety() {
        return variety;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Field f)) return false;
        return harvestSize == f.harvestSize &&
                harvestDate.equals(f.harvestDate) &&
                variety.equals(f.variety);
    }
    @Override
    public int hashCode() {
        return Objects.hash(harvestSize, harvestDate, variety);
    }

    @Override
    public String toString() {
        return "Field{" +
                "id=" + id +
                ", harvestSize=" + harvestSize +
                ", harvestDate=" + harvestDate +
                ", variety='" + variety + '\'' +
                '}';
    }
}
