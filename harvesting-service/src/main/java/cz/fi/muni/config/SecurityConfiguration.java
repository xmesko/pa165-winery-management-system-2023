package cz.fi.muni.config;


import io.swagger.v3.oas.models.security.*;
import org.springdoc.core.customizers.OpenApiCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.resource.OAuth2ResourceServerConfigurer;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebSecurity
@EnableWebMvc
public class SecurityConfiguration {


    @Bean
    public OpenApiCustomizer openAPICustomizer() {
        return openApi -> {
            openApi.getComponents()
                    .addSecuritySchemes("Bearer",
                            new SecurityScheme()
                                    .type(SecurityScheme.Type.HTTP)
                                    .scheme("bearer")
                                    .description("provide a valid access token")
                    );
            var managerScope = new SecurityRequirement().addList("Bearer", "test_write");
            var userScope = new SecurityRequirement().addList("Bearer", "test_read");

            openApi.getPaths().get("/rawgrape").getPost().addSecurityItem(managerScope);
            openApi.getPaths().get("/rawgrape/{id}").getGet().addSecurityItem(userScope);
            openApi.getPaths().get("/rawgrape/{id}").getPatch().addSecurityItem(managerScope);
            openApi.getPaths().get("/rawgrape/{id}").getDelete().addSecurityItem(managerScope);
            openApi.getPaths().get("/rawgrapes").getGet().addSecurityItem(userScope);


            openApi.getPaths().get("/field").getPost().addSecurityItem(managerScope);
            openApi.getPaths().get("/field/{id}").getGet().addSecurityItem(userScope);
            openApi.getPaths().get("/field/{id}").getPatch().addSecurityItem(managerScope);
            openApi.getPaths().get("/field/{id}").getDelete().addSecurityItem(managerScope);
            openApi.getPaths().get("/field/{id}").getPost().addSecurityItem(managerScope);
            openApi.getPaths().get("/fields").getGet().addSecurityItem(userScope);
        };
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.authorizeHttpRequests(x -> x
                        .requestMatchers("/swagger-ui/**", "/v3/api-docs/**", "/").permitAll()

                        .requestMatchers(HttpMethod.DELETE, "/rawgrape/**").hasAuthority("SCOPE_test_write")
                        .requestMatchers(HttpMethod.PATCH, "/rawgrape/**").hasAuthority("SCOPE_test_write")
                        .requestMatchers(HttpMethod.GET, "/rawgrape/**").hasAuthority("SCOPE_test_read")
                        .requestMatchers("/rawgrapes").hasAuthority("SCOPE_test_read")
                        .requestMatchers("/rawgrape").hasAuthority("SCOPE_test_write")

                        .requestMatchers(HttpMethod.DELETE, "/field/**").hasAuthority("SCOPE_test_write")
                        .requestMatchers(HttpMethod.POST, "/field/**").hasAuthority("SCOPE_test_write")
                        .requestMatchers(HttpMethod.PATCH, "/field/**").hasAuthority("SCOPE_test_write")
                        .requestMatchers(HttpMethod.GET, "/field/**").hasAuthority("SCOPE_test_read")
                        .requestMatchers("/fields").hasAuthority("SCOPE_test_read")
                        .requestMatchers("/field").hasAuthority("SCOPE_test_write")
                        .anyRequest().permitAll()
                )
                .oauth2ResourceServer(OAuth2ResourceServerConfigurer::opaqueToken)
        ;
        return http.build();
    }

}
