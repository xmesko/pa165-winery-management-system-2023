package cz.fi.muni.service;

import cz.fi.muni.data.model.Field;
import cz.fi.muni.data.repository.FieldRepository;
import cz.fi.muni.exceptions.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.List;


@Service
public class FieldService implements IFieldService{
    private static final Logger log = LoggerFactory.getLogger(FieldService.class);
    private final FieldRepository fieldRepository;
    @Autowired
    public FieldService(FieldRepository fieldRepository) {
        this.fieldRepository = fieldRepository;
    }
    @Override
    @Transactional(readOnly = true)
    public Field findById(Long id) {
        return fieldRepository
                .findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Field with id: " + id + " was not found."));
    }

    @Override
    @Transactional(readOnly = true)
    public List<Field> findAll() {
        return fieldRepository.findAll();
    }

    @Override
    @Transactional
    public void editField(Long id, Field field) {
        Field fieldToEdit = findById(id);
        if (fieldToEdit == null) {
            log.info("Could not find a Field with this id .");
            throw new ResourceNotFoundException("Field with id: " + id + " was not found.");
        }
        fieldToEdit.setHarvestDate(field.getHarvestDate());
        fieldToEdit.setHarvestSize(field.getHarvestSize());
        fieldToEdit.setVariety(field.getVariety());
        fieldRepository.save(fieldToEdit);

    }

    @Override
    public Long harvestField(Long id) {
        Field field = fieldRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Field with id: " + id + " was not found."));
        // retrieve harvest date from field add one year and set it as new harvest date
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(field.getHarvestDate());
        calendar.add(Calendar.YEAR, 1);
        field.setHarvestDate(calendar.getTime());
        fieldRepository.save(field);

        // add a new RawGrapes to rawGrapesRepository via FieldFacade
        return field.getId();
    }

    @Override
    public Long addField(Field field) {
        log.info("SERVICE:Adding a new Field: " + field.toString());
        fieldRepository.save(field);
        log.info("SERVICE:Added a new Field: " + field);
        return field.getId();

    }

    @Override
    public void deleteField(Long id) {
        fieldRepository.deleteById(id);
    }
}
