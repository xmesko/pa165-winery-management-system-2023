package cz.fi.muni.service;

import cz.fi.muni.data.model.Field;


import java.util.List;

public interface IFieldService {
    Field findById(Long id);
    List<Field> findAll();
    void editField(Long id, Field field);
    Long harvestField(Long id);
    Long addField(Field field);
    void deleteField(Long id);
}
