package cz.fi.muni.service;

import cz.fi.muni.data.model.RawGrapes;
import cz.fi.muni.data.repository.RawGrapesRepository;
import cz.fi.muni.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.logging.Logger;

@Service
public class RawGrapesService implements IRawGrapesService{
    private static final Logger log = Logger.getLogger(RawGrapesService.class.getName());
    private final RawGrapesRepository rawGrapesRepository;

    @Autowired
    public RawGrapesService(RawGrapesRepository rawGrapesRepository) {
        this.rawGrapesRepository = rawGrapesRepository;
    }
    @Override
    @Transactional(readOnly = true)
    public RawGrapes findById(Long id) {
        return rawGrapesRepository
                .findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("RawGrapes with id: " + id + " was not found."));

    }

    @Override
    @Transactional(readOnly = true)
    public List<RawGrapes> findAll() {
        return rawGrapesRepository.findAll();
    }

    @Override
    @Transactional
    public void editRawGrapes(Long id, RawGrapes rawGrapes) {
        RawGrapes rawGrapesToEdit = findById(id);
        if (rawGrapesToEdit == null) {
            log.info("Could not find a RawGrapes with this id .");
            throw new ResourceNotFoundException("RawGrapes with id: " + id + " was not found.");
        }
        rawGrapesToEdit.setAmount(rawGrapes.getAmount());
        rawGrapesToEdit.setVariety(rawGrapes.getVariety());
        rawGrapesToEdit.setQuality(rawGrapes.getQuality());
        rawGrapesRepository.save(rawGrapesToEdit);
    }


    @Override
    @Transactional
    public Long addRawGrapes(RawGrapes rawGrapes) {
        rawGrapesRepository.save(rawGrapes);
        return rawGrapes.getId();
    }

    @Override
    @Transactional
    public void deleteRawGrapes(Long id) {
        rawGrapesRepository.deleteById(id);

    }

}
