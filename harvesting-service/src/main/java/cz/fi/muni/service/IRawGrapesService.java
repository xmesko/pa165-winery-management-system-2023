package cz.fi.muni.service;

import cz.fi.muni.data.model.RawGrapes;


import java.util.List;
public interface IRawGrapesService {
    RawGrapes findById(Long id);
    List<RawGrapes> findAll();
    void editRawGrapes(Long id, RawGrapes rawGrapes);
    Long addRawGrapes(RawGrapes rawGrapes);
    void deleteRawGrapes(Long id);
}
