package cz.fi.muni.rest;

import cz.fi.muni.facade.IRawGrapesFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import cz.fi.muni.harvesting.api.RawGrapeApiDelegate;
import cz.fi.muni.harvesting.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.List;

@Component
@EnableWebMvc

public class RawGrapeController implements RawGrapeApiDelegate {
    private static final Logger log = LoggerFactory.getLogger(RawGrapeController.class);
    private final IRawGrapesFacade rawGrapesFacade;


    @Autowired
    public RawGrapeController(IRawGrapesFacade rawGrapesFacade) {
        this.rawGrapesFacade = rawGrapesFacade;
    }

    @Override
    public ResponseEntity<AddRawGrapeResponse> addRawGrape(NewRawGrape newRawGrape) {
        if (newRawGrape == null) {
            log.info("No New Raw Grape to be added provided.");
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        Long newRawGrapeLong = rawGrapesFacade.addRawGrapes(newRawGrape);
        // Handle bad grape
        if (newRawGrapeLong == null) {
            log.info("Could not create a new Raw Grape");
            return new ResponseEntity<>(null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
        AddRawGrapeResponse addRawGrapeResponse = new AddRawGrapeResponse() {
            {
                message(newRawGrapeLong.toString());
            }
        };
        return new ResponseEntity<>(addRawGrapeResponse, HttpStatus.CREATED);
    }
    @Override
    public ResponseEntity<RawGrape> getRawGrapeDetails(String id) {
        if (id == null) {
            log.info("Provided Raw Grape id is null.");
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        // retrieve raw grape from DB
        RawGrape rawGrape = rawGrapesFacade.findById(Long.parseLong(id));

        // handle non-existing id
        if (rawGrape == null) {
            log.info("Could not find a Raw Grape with this id .");
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(rawGrape, HttpStatus.OK);
    }
    @Override
    public ResponseEntity<Void> deleteRawGrape(String id) {
        if (id == null) {
            log.info("Provided Raw Grape id is null.");
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        // Delete from DB
        rawGrapesFacade.deleteRawGrapes(Long.parseLong(id));
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> editRawGrape(String id, EditRawGrape editRawGrape) {
        if (id == null) {
            log.info("Provided Raw Grape id is null.");
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        rawGrapesFacade.editRawGrapes(Long.parseLong(id), editRawGrape);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<RawGrape>> getAllRawGrapes() {
        // Retrieve all RawGrapes from DB
        log.info("getAllRawGrapes");
        List<RawGrape> grapes = rawGrapesFacade.findAll();
        return new ResponseEntity<>(grapes, HttpStatus.OK);
    }
}
