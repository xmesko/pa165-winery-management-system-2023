package cz.fi.muni.rest;

import cz.fi.muni.facade.IFieldFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import cz.fi.muni.harvesting.api.FieldApiDelegate;
import cz.fi.muni.harvesting.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.List;

@Component
@EnableWebMvc
public class FieldController implements FieldApiDelegate {
    private static final Logger log = LoggerFactory.getLogger(FieldController.class);
    private final IFieldFacade fieldFacade;


    @Autowired
    public FieldController(IFieldFacade fieldFacade) {
        this.fieldFacade = fieldFacade;
    }

    @Override
    public ResponseEntity<NewFieldResponse> newField(NewField newField) {
        if (newField == null) {
            log.info("No New Field to be added provided.");
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        log.info("REST:Adding a new Field: " + newField.toString());
        Long newFieldID = fieldFacade.addField(newField);
        if (newFieldID == null) {
            log.info("Could not create a new Field");
            return new ResponseEntity<>(null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
        NewFieldResponse newFieldResponse = new NewFieldResponse() {
            {
                message(newFieldID.toString());
            }
        };
        return new ResponseEntity<>(newFieldResponse, HttpStatus.CREATED);
    }
    @Override
    public ResponseEntity<Field> getField(String id) {
        if (id == null) {
            log.info("Provided Field id is null.");
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        // retrieve field from DB
        Field field = fieldFacade.findById(Long.parseLong(id));

        // handle non-existing id
        if (field == null) {
            log.info("Could not find a Field with this id .");
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(field, HttpStatus.OK);
    }
    @Override
    public ResponseEntity<Void> deleteField(String id) {
        if (id == null) {
            log.info("Provided Field id is null.");
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        // Remove from DB
        fieldFacade.deleteField(Long.parseLong(id));
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> editField(String id, EditField editField) {
        if (id == null) {
            log.info("Provided Field id is null.");
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        fieldFacade.editField(Long.parseLong(id), editField);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<HarvestedFieldResponse> harvestField(String id) {

        if (id == null) {
            log.info("No Field id provided.");
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        // Harvest
        log.info("Harvesting field");
        // Check if id is valid
        Long newlyHarvestedRawGrapeId = fieldFacade.harvestField(Long.parseLong(id));

        log.info("harvested");

        // Handle bad Field
        if (newlyHarvestedRawGrapeId == null) {
            log.info("Failed to harvest Field");
            return new ResponseEntity<>(null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
        HarvestedFieldResponse harvestedFieldResponse = new HarvestedFieldResponse() {
            {
                message(newlyHarvestedRawGrapeId.toString());
            }
        };
        return new ResponseEntity<>(harvestedFieldResponse, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<Field>> getAllFields() {
        // Retrieve all Fields from DB
        log.info("getAllFields");
        List<Field> fields = fieldFacade.findAll();
        return new ResponseEntity<>(fields, HttpStatus.OK);
    }
}
