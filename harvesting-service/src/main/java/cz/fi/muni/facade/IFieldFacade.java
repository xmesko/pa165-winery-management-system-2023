package cz.fi.muni.facade;

import cz.fi.muni.harvesting.model.EditField;
import cz.fi.muni.harvesting.model.Field;
import cz.fi.muni.harvesting.model.NewField;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IFieldFacade {
    public Field findById(Long id);
    public List<Field> findAll();
    void editField(Long id, EditField field);
    void deleteField(Long id);
    Long addField(NewField newField);
    Long harvestField(Long id);
}
