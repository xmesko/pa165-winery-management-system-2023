package cz.fi.muni.facade;

import cz.fi.muni.harvesting.model.EditRawGrape;
import cz.fi.muni.harvesting.model.NewRawGrape;
import cz.fi.muni.harvesting.model.RawGrape;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IRawGrapesFacade {
    void editRawGrapes(Long id, EditRawGrape rawGrapes);
    Long addRawGrapes(NewRawGrape rawGrapes);
    void deleteRawGrapes(Long id);
    RawGrape findById(Long id);
    List<RawGrape> findAll();
}
