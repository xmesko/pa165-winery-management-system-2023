package cz.fi.muni.facade;

import cz.fi.muni.data.model.RawGrapes;
import cz.fi.muni.harvesting.model.EditField;
import cz.fi.muni.harvesting.model.Field;
import cz.fi.muni.harvesting.model.NewField;

import cz.fi.muni.mappers.FieldMapper;
import cz.fi.muni.service.FieldService;
import cz.fi.muni.service.RawGrapesService;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Transactional
public class FieldFacade implements IFieldFacade {
    private final FieldService fieldService;
    private final FieldMapper fieldMapper;
    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(FieldFacade.class);
    private final RawGrapesService rawGrapesService;

    @Autowired
    public FieldFacade(FieldService fieldService, RawGrapesService rawGrapesService, FieldMapper fieldMapper) {
        this.fieldService = fieldService;
        this.rawGrapesService = rawGrapesService;
        this.fieldMapper = fieldMapper;
    }


    @Override
    public Field findById(Long id) {
        return fieldMapper.mapToOutField(fieldService.findById(id));
    }

    @Override
    public List<Field> findAll() {
        return fieldMapper.mapToOutFieldList(fieldService.findAll());
    }

    @Override
    public void editField(Long id, EditField field) {
        fieldService.editField(id, fieldMapper.mapToInField(field));
    }

    @Override
    public void deleteField(Long id) {
        fieldService.deleteField(id);
    }

    @Override
    public Long addField(NewField newField) {
        log.info("FACADE:Adding a new Field: " + newField.toString());
        cz.fi.muni.data.model.Field field = fieldMapper.mapToInField(newField);
        log.info("MAPPER:Adding a new Field: " + field.toString());
        return fieldService.addField(field);
    }


    @Override
    public Long harvestField(Long id) {
        Long harvestFieldId = fieldService.harvestField(id);
        cz.fi.muni.data.model.Field field = fieldService.findById(harvestFieldId);
        RawGrapes rawGrapes = new RawGrapes(field.getHarvestSize(), field.getVariety(),"Normal");
        return rawGrapesService.addRawGrapes(rawGrapes);
    }
}
