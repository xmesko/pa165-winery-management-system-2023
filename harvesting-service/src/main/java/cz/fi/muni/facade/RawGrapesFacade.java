package cz.fi.muni.facade;

import cz.fi.muni.harvesting.model.EditRawGrape;
import cz.fi.muni.harvesting.model.NewRawGrape;
import cz.fi.muni.harvesting.model.RawGrape;
import cz.fi.muni.mappers.RawGrapesMapper;
import cz.fi.muni.service.RawGrapesService;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Transactional
public class RawGrapesFacade implements IRawGrapesFacade{
    private final RawGrapesService rawGrapesService;
    private final RawGrapesMapper rawGrapesMapper;

    @Autowired
    public RawGrapesFacade(RawGrapesService rawGrapesService, RawGrapesMapper rawGrapesMapper) {
        this.rawGrapesService = rawGrapesService;
        this.rawGrapesMapper = rawGrapesMapper;
    }

    @Override
    public void editRawGrapes(Long id, EditRawGrape rawGrapes) {
        rawGrapesService.editRawGrapes(id, rawGrapesMapper.mapToInRawGrapes(rawGrapes));
    }

    @Override
    public Long addRawGrapes(NewRawGrape rawGrapes) {
        return rawGrapesService.addRawGrapes(rawGrapesMapper.mapToInRawGrapes(rawGrapes));
    }

    @Override
    public void deleteRawGrapes(Long id) {
        rawGrapesService.deleteRawGrapes(id);
    }

    @Override
    public RawGrape findById(Long id) {
        return rawGrapesMapper.mapToOutRawGrapes(rawGrapesService.findById(id));
    }

    @Override
    public List<RawGrape> findAll() {
        return rawGrapesMapper.mapToOutRawGrapesList(rawGrapesService.findAll());
    }
}
