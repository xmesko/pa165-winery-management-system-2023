package cz.fi.muni.mappers;
import cz.fi.muni.data.model.Field;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(componentModel = "spring")
public interface FieldMapper {
    @Mapping(target = "id", ignore = true)

    Field mapToInField(cz.fi.muni.harvesting.model.Field field);
    @Mapping(target = "id", ignore = true)
    Field mapToInField(cz.fi.muni.harvesting.model.NewField field);
    @Mapping(target = "id", ignore = true)

    Field mapToInField(cz.fi.muni.harvesting.model.EditField field);
    cz.fi.muni.harvesting.model.Field mapToOutField(Field field);
    List<cz.fi.muni.harvesting.model.Field> mapToOutFieldList(List<Field> fields);
}
