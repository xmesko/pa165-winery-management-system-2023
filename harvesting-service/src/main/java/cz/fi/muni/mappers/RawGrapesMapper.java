package cz.fi.muni.mappers;

import cz.fi.muni.data.model.RawGrapes;
import cz.fi.muni.harvesting.model.EditRawGrape;
import cz.fi.muni.harvesting.model.NewRawGrape;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(componentModel = "spring")
public interface RawGrapesMapper {

    @Mapping(target = "id", ignore = true)
    RawGrapes mapToInRawGrapes(cz.fi.muni.harvesting.model.RawGrape rawGrapes);
    @Mapping(target = "id", ignore = true)
    RawGrapes mapToInRawGrapes(NewRawGrape rawGrapes);

    @Mapping(target = "id", ignore = true)
    RawGrapes mapToInRawGrapes(EditRawGrape rawGrapes);

    cz.fi.muni.harvesting.model.RawGrape mapToOutRawGrapes(RawGrapes byId);

    List<cz.fi.muni.harvesting.model.RawGrape> mapToOutRawGrapesList(List<RawGrapes> all);


}
