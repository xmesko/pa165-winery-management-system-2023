package cz.fi.muni.rest;

import cz.fi.muni.data.repository.FieldRepository;
import cz.fi.muni.data.repository.RawGrapesRepository;
import cz.fi.muni.facade.FieldFacade;
import cz.fi.muni.facade.RawGrapesFacade;
import cz.fi.muni.service.FieldService;
import cz.fi.muni.service.RawGrapesService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.servlet.FilterChain;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import java.util.List;
import static cz.fi.muni.shared.Utils.asJsonString;
import static org.mockito.Mockito.verify;
import cz.fi.muni.harvesting.model.Field;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import cz.fi.muni.harvesting.model.EditField;
import cz.fi.muni.harvesting.model.NewField;

@WebMvcTest(controllers = {FieldController.class, RawGrapeController.class})
@AutoConfigureMockMvc(addFilters = false)
@ExtendWith(MockitoExtension.class)
public class FieldControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RawGrapesRepository rawGrapesRepository;
    @MockBean
    private FieldRepository fieldRepository;
    @MockBean
    private RawGrapesService rawGrapesService;
    @MockBean
    private FieldService fieldService;
    @MockBean
    private FieldFacade fieldFacade;
    @MockBean
    private RawGrapesFacade rawGrapesFacade;

    private static final Field expectedField = new Field();
    private static final EditField editField = new EditField();
    private static final NewField newField = new NewField();

    private static final List<Field> expectedFields = List.of(expectedField);

    private static final Long harvestedRawGrapesId = 123L;

    @BeforeAll
    static void initAll() {
        expectedField.setId(1);
        expectedField.setHarvestSize(200);
        expectedField.setVariety("variety");
    }

    @Test
    void getField_IdIsNull_ReturnsNotFoundStatus() throws Exception {
        mockMvc.perform(get("/field/"))
                .andExpect(status().isNotFound());
    }

    @Test
    void getField_FieldExists_ReturnExpectedField() throws Exception {
        when(fieldFacade.findById(1L)).thenReturn(expectedField);

        mockMvc.perform(get("/field/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(expectedField.getId()))
                .andExpect(jsonPath("$.variety").value(expectedField.getVariety()));
    }

    @Test
    void getAllFields_FieldsFound_ReturnsFoundFieldsAndStatusIsOk() throws Exception {
        when(fieldFacade.findAll()).thenReturn(expectedFields);
        mockMvc.perform(get("/fields"))
                .andExpect(jsonPath("$[0].id").value(expectedField.getId()))
                .andExpect(status().isOk());

        verify(fieldFacade).findAll();
    }

    @Test
    void harvestField_FieldIsHarvestable_ReturnsIdInMessage() throws Exception {
        when(fieldFacade.harvestField(1L)).thenReturn(harvestedRawGrapesId);
        mockMvc.perform(post("/field/1").contentType("application/json"))
                .andExpect(status().isCreated())
                .andExpect(content().string("{\"message\":\"123\"}"));
    }

    @Test
    void harvestField_FieldIsNotHarvestable_ReturnsUnprocessableEntity() throws Exception {
        when(fieldFacade.harvestField(1L)).thenReturn(null);
        mockMvc.perform(post("/field/1").contentType("application/json"))
                .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void harvestField_IdIsNull_ReturnsNotFoundStatus() throws Exception {
        mockMvc.perform(post("/field/").contentType("application/json"))
                .andExpect(status().isNotFound());
    }

    @Test
    void editField_IdIsNull_ReturnsNotFound() throws Exception {
        mockMvc.perform(patch("/field/")).andExpect(status().isNotFound());
    }

    @Test
    void editField_FieldExists_FieldIsUpdated() throws Exception {
        editField.setId(1);
        editField.setVariety("new variety");
        mockMvc.perform(patch("/field/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(editField)))
                .andExpect(status().isOk());

        verify(fieldFacade).editField(1L, editField);
    }

    @Test
    void deleteField_IdIsNull_ReturnsNotFound() throws Exception {
        mockMvc.perform(delete("/field/")).andExpect(status().isNotFound());
    }

    @Test
    void deleteField_FieldNotFound_ReturnsNotFound() throws Exception {
        mockMvc.perform(delete("/field/78")).andExpect(status().isOk());
    }

    @Test
    void deleteField_FieldFound_ReturnsNotFound() throws Exception {
        mockMvc.perform(delete("/field/789")).andExpect(status().isOk());
    }

    @Test
    void newField_NewFieldIsNull_ReturnsBadRequest() throws Exception {
        mockMvc.perform(post("/field")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(null)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void newField_NewFieldIsValid_ReturnsNewId() throws Exception {
        newField.setVariety("new variety");
        newField.setHarvestSize(200);
        when(fieldFacade.addField(newField)).thenReturn(159L);
        mockMvc.perform(post("/field")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(newField)))
                .andExpect(status().isCreated())
                .andExpect(content().string("{\"message\":\"159\"}"));

        verify(fieldFacade).addField(newField);
    }

    @Test
    void newField_CouldNotBeCreated_ReturnsUnprocessableEntityStatus() throws Exception {
        newField.setVariety("new variety");
        newField.setHarvestSize(200);
        when(fieldFacade.addField(newField)).thenReturn(null);
        mockMvc.perform(post("/field")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(newField)))
                .andExpect(status().isUnprocessableEntity());

        verify(fieldFacade).addField(newField);
    }
}
