package cz.fi.muni.rest;

import cz.fi.muni.data.repository.FieldRepository;
import cz.fi.muni.data.repository.RawGrapesRepository;
import cz.fi.muni.facade.FieldFacade;
import cz.fi.muni.facade.RawGrapesFacade;
import cz.fi.muni.service.FieldService;
import cz.fi.muni.service.RawGrapesService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import java.util.List;
import static cz.fi.muni.shared.Utils.asJsonString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import cz.fi.muni.harvesting.model.RawGrape;
import cz.fi.muni.harvesting.model.EditRawGrape;
import cz.fi.muni.harvesting.model.NewRawGrape;

@WebMvcTest(controllers = RawGrapeController.class)
@AutoConfigureMockMvc(addFilters = false)
@ExtendWith(MockitoExtension.class)
public class RawGrapeControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RawGrapesRepository rawGrapesRepository;
    @MockBean
    private FieldRepository fieldRepository;
    @MockBean
    private RawGrapesService rawGrapesService;
    @MockBean
    private FieldService fieldService;
    @MockBean
    private RawGrapesFacade rawGrapesFacade;

    private static final RawGrape expectedRawGrape = new RawGrape();
    private static final List<RawGrape> expectedRawGrapes = List.of(expectedRawGrape);
    private static final EditRawGrape editRawGrape = new EditRawGrape();
    private static final NewRawGrape newRawGrape = new NewRawGrape();

    @BeforeAll
    static void initAll() {
        expectedRawGrape.setId(123);
        expectedRawGrape.setAmount(200);
        expectedRawGrape.setVariety("variety");
    }

    @Test
    void getAllRawGrapes_RawGrapesExists_ReturnsRawGrapes() throws Exception {
        when(rawGrapesFacade.findAll()).thenReturn(expectedRawGrapes);
        mockMvc.perform(get("/rawgrapes"))
                .andExpect(jsonPath("$[0].id").value(expectedRawGrape.getId()))
                .andExpect(status().isOk());

        verify(rawGrapesFacade).findAll();
    }

    @Test
    void getRawGrapeDetails_IdIsNull_ReturnsNotFound() throws Exception {
        mockMvc.perform(get("/rawgrape/"))
                .andExpect(status().isNotFound());
    }

    @Test
    void getRawGrapeDetails_RawGrapeExists_ReturnsRawGrape() throws Exception {
        when(rawGrapesFacade.findById(123L)).thenReturn(expectedRawGrape);

        mockMvc.perform(get("/rawgrape/123"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(expectedRawGrape.getId()))
                .andExpect(jsonPath("$.variety").value(expectedRawGrape.getVariety()));
    }

    @Test
    void deleteRawGrapes_IdIsNull_ReturnsNotFound() throws Exception {
        mockMvc.perform(delete("/rawgrape/")).andExpect(status().isNotFound());
    }

    @Test
    void deleteRawGrapes_GrapeFound_ReturnsNotFound() throws Exception {
        mockMvc.perform(delete("/rawgrape/789")).andExpect(status().isOk());
    }

    @Test
    void deleteRawGrapes_GrapeNotFound_ReturnsNotFound() throws Exception {
        mockMvc.perform(delete("/rawgrape/7894")).andExpect(status().isOk());
    }

    @Test
    void editRawGrape_IdIsNull_ReturnsNotFound() throws Exception {
        mockMvc.perform(patch("/rawgrape/")).andExpect(status().isNotFound());
    }

    @Test
    void addRawGrape_RequestBodyIsNull_ReturnsBadRequest() throws Exception {
        mockMvc.perform(post("/rawgrape")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(null)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void addRawGrape_CouldNotBeCreated_ReturnsUnprocessableEntityStatus() throws Exception {
        newRawGrape.setVariety("new variety");
        newRawGrape.setAmount(200);
        when(rawGrapesFacade.addRawGrapes(newRawGrape)).thenReturn(null);
        mockMvc.perform(post("/rawgrape")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(newRawGrape)))
                .andExpect(status().isUnprocessableEntity());

        verify(rawGrapesFacade).addRawGrapes(newRawGrape);
    }

    @Test
    void addRawGrape_NewRawGrapeIsValid_ReturnsNewId() throws Exception {
        newRawGrape.setVariety("new variety");
        newRawGrape.setAmount(200);
        when(rawGrapesFacade.addRawGrapes(newRawGrape)).thenReturn(159L);
        mockMvc.perform(post("/rawgrape")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(newRawGrape)))
                .andExpect(status().isCreated())
                .andExpect(content().string("{\"message\":\"159\"}"));

        verify(rawGrapesFacade).addRawGrapes(newRawGrape);
    }
}
