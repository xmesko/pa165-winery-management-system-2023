package cz.fi.muni.facade;

import cz.fi.muni.data.model.RawGrapes;

import cz.fi.muni.mappers.RawGrapesMapper;
import cz.fi.muni.service.RawGrapesService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RawGrapesFacadeTest {
    @Mock
    private RawGrapesService rawGrapesServiceMck;
    @Mock
    private RawGrapesMapper rawGrapesMapperMck;
    @InjectMocks
    private RawGrapesFacade rawGrapesFacade;

    private final Integer fieldSize = 450;
    private static final Long rawGrapesId = 456L;
    private final RawGrapes rawGrapes = new RawGrapes(fieldSize, "test variety", "test quality");
    private final cz.fi.muni.harvesting.model.RawGrape expectedRawGrapes = new cz.fi.muni.harvesting.model.RawGrape();


    @BeforeEach
    void setUp() {
        expectedRawGrapes.setVariety("test variety");
        expectedRawGrapes.setAmount(fieldSize);
    }

    @Test
    void editRawGrapes() {
        cz.fi.muni.harvesting.model.EditRawGrape editRawGrapes = new cz.fi.muni.harvesting.model.EditRawGrape();
        editRawGrapes.setVariety("new test variety");
        editRawGrapes.setAmount(300);
        rawGrapes.setVariety("new test variety");
        rawGrapes.setAmount(300);
        when(rawGrapesMapperMck.mapToInRawGrapes(editRawGrapes)).thenReturn(rawGrapes);

        rawGrapesFacade.editRawGrapes(rawGrapesId, editRawGrapes);

        verify(rawGrapesServiceMck).editRawGrapes(rawGrapesId, rawGrapes);
    }

    @Test
    void addRawGrapes() {
        cz.fi.muni.harvesting.model.NewRawGrape newRawGrapes = new cz.fi.muni.harvesting.model.NewRawGrape();
        newRawGrapes.setVariety("test variety");
        newRawGrapes.setAmount(fieldSize);
        when(rawGrapesMapperMck.mapToInRawGrapes(newRawGrapes)).thenReturn(rawGrapes);
        when(rawGrapesServiceMck.addRawGrapes(rawGrapes)).thenReturn(rawGrapesId);

        var actualRawGrapesId = rawGrapesFacade.addRawGrapes(newRawGrapes);

        assertEquals(rawGrapesId, actualRawGrapesId);
    }

    @Test
    void deleteRawGrapes() {
        rawGrapesFacade.deleteRawGrapes(rawGrapesId);

        verify(rawGrapesServiceMck).deleteRawGrapes(rawGrapesId);
    }

    @Test
    void findById() {
        when(rawGrapesServiceMck.findById(rawGrapesId)).thenReturn(rawGrapes);
        when(rawGrapesMapperMck.mapToOutRawGrapes(rawGrapes)).thenReturn(expectedRawGrapes);

        var actualRawGrapes = rawGrapesFacade.findById(rawGrapesId);
        assertEquals(expectedRawGrapes, actualRawGrapes);
    }

    @Test
    void findAll() {
        List<cz.fi.muni.harvesting.model.RawGrape> expectedRawGrapesList = List.of(expectedRawGrapes);
        when(rawGrapesServiceMck.findAll()).thenReturn(java.util.List.of(rawGrapes));
        when(rawGrapesMapperMck.mapToOutRawGrapesList(java.util.List.of(rawGrapes))).thenReturn(expectedRawGrapesList);

        var actualRawGrapes = rawGrapesFacade.findAll();

        assertEquals(expectedRawGrapesList, actualRawGrapes);
    }
}
