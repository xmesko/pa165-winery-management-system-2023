package cz.fi.muni.facade;

import cz.fi.muni.data.model.Field;
import cz.fi.muni.mappers.FieldMapper;
import cz.fi.muni.service.FieldService;
import cz.fi.muni.service.RawGrapesService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import cz.fi.muni.data.model.RawGrapes;
import org.mockito.Mock;
import org.junit.jupiter.api.Test;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class FieldFacadeTest {
    @Mock
    private FieldService fieldServiceMck;

    @Mock
    private RawGrapesService rawGrapesServiceMck;
    @Mock
    private FieldMapper fieldMapperMck;

    @InjectMocks
    private FieldFacade fieldFacade;

    private static final Long fieldId = 123L;
    private final Field field = new Field();
    private final cz.fi.muni.harvesting.model.Field expectedField = new cz.fi.muni.harvesting.model.Field();
    private final cz.fi.muni.harvesting.model.NewField newField = new cz.fi.muni.harvesting.model.NewField();

    private final Integer fieldSize = 200;

    private static final Long rawGrapesId = 456L;
    private final RawGrapes rawGrapes = new RawGrapes(fieldSize, "variety", "Normal");


    @BeforeEach
    void setUp() {
        field.setId(fieldId);
        field.setHarvestSize(fieldSize);
        field.setVariety("variety");
        expectedField.setId(Math.toIntExact(fieldId));
        expectedField.setHarvestSize(fieldSize);
        expectedField.setVariety("variety");
    }

    @Test
    void findById() {
        when(fieldServiceMck.findById(fieldId)).thenReturn(field);
        when(fieldMapperMck.mapToOutField(field)).thenReturn(expectedField);

        var actualField = fieldFacade.findById(fieldId);
        assertEquals(expectedField, actualField);
    }

    @Test
    void findAll() {
        List<Field> fields = List.of(field);
        List<cz.fi.muni.harvesting.model.Field> expectedFields = List.of(expectedField);

        when(fieldServiceMck.findAll()).thenReturn(fields);
        when(fieldMapperMck.mapToOutFieldList(fields)).thenReturn(expectedFields);

        var actualFields = fieldFacade.findAll();
        assertEquals(expectedFields, actualFields);
    }

    @Test
    void editField() {
        var editField = new cz.fi.muni.harvesting.model.EditField();
        editField.setVariety("new variety");
        editField.setHarvestSize(300);
        field.setVariety(editField.getVariety());
        field.setHarvestSize(editField.getHarvestSize());
        when(fieldMapperMck.mapToInField(editField)).thenReturn(field);

        fieldFacade.editField(fieldId, editField);

        assertEquals(editField.getVariety(), field.getVariety());
        assertEquals(editField.getHarvestSize(), field.getHarvestSize());
    }

    @Test
    void deleteField() {
        fieldFacade.deleteField(fieldId);

        verify(fieldServiceMck).deleteField(fieldId);
    }

    @Test
    void addField() {
        newField.setVariety("add test variety");
        newField.setHarvestSize(300);
        Field createdField = new Field(300, null, "add test variety");
        Long expectedId = createdField.getId();
        when(fieldMapperMck.mapToInField(newField)).thenReturn(createdField);
        when(fieldServiceMck.addField(createdField)).thenReturn(expectedId);

        var actualFieldId = fieldFacade.addField(newField);

        assertEquals(expectedId, actualFieldId);
    }

    @Test
    void harvestField() {
        when(fieldServiceMck.harvestField(fieldId)).thenReturn(fieldId);
        when(fieldServiceMck.findById(fieldId)).thenReturn(field);
        when(rawGrapesServiceMck.addRawGrapes(rawGrapes)).thenReturn(rawGrapesId);

        var actualRawGrapeId = fieldFacade.harvestField(fieldId);

        assertEquals(rawGrapesId, actualRawGrapeId);
    }



}
