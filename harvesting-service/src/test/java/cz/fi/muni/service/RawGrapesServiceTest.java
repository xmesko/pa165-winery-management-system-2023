package cz.fi.muni.service;

import cz.fi.muni.data.repository.RawGrapesRepository;
import cz.fi.muni.exceptions.ResourceNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import cz.fi.muni.data.model.RawGrapes;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class RawGrapesServiceTest {
    @Mock
    private RawGrapesRepository rawGrapesRepositoryMck;
    @InjectMocks
    private RawGrapesService rawGrapesService;

    private RawGrapes rawGrapes;

    @BeforeEach
    void setUp() {
        rawGrapes = new RawGrapes(352, "test", "test quality");
        rawGrapes.setId(123L);
    }

    @Test
    void findById_RawGrapesNotFound_ThrowsNotFound() {
        when(rawGrapesRepositoryMck.findById(123L)).thenReturn(Optional.empty());

        Exception exception = assertThrows(ResourceNotFoundException.class, () -> {
            rawGrapesService.findById(123L);
        });

        assertTrue(exception.getMessage().contains("RawGrapes with id: 123 was not found."));
    }

    @Test
    void findById_RawGrapesFound_ReturnsRawGrapes() {
        when(rawGrapesRepositoryMck.findById(123L)).thenReturn(Optional.ofNullable(rawGrapes));

        var actualGrape = rawGrapesService.findById(123L);

        assertEquals(rawGrapes, actualGrape);
    }

    @Test
    void findAll_RawGrapesFound_ReturnsRawGrapes() {
        when(rawGrapesRepositoryMck.findAll()).thenReturn(List.of(rawGrapes));

        var returnedGrapes = rawGrapesService.findAll();

        assertEquals(returnedGrapes, List.of(rawGrapes));
    }

    @Test
    void editRawGrapes_IdIsNotFound_ThrowsResourceNotFound() {
        when(rawGrapesRepositoryMck.findById(123L)).thenReturn(Optional.empty());

        Exception exception = assertThrows(ResourceNotFoundException.class, () -> {
            rawGrapesService.editRawGrapes(rawGrapes.getId(), null);
        });

        assertTrue(exception.getMessage().contains("RawGrapes with id: 123 was not found."));
    }

    @Test
    void editRawGrapes_EditGrapeIsValid_ResourceIsUpdated() {
        RawGrapes updatedGrape = new RawGrapes(159, "test variety", "test quality");
        updatedGrape.setId(rawGrapes.getId());
        when(rawGrapesRepositoryMck.findById(123L)).thenReturn(Optional.ofNullable(rawGrapes));

        rawGrapesService.editRawGrapes(rawGrapes.getId(), updatedGrape);

        var actualUpdatedGrape = rawGrapesService.findById(rawGrapes.getId());
        assertEquals(updatedGrape.getAmount(), actualUpdatedGrape.getAmount());
        assertEquals(updatedGrape.getVariety(), actualUpdatedGrape.getVariety());
    }

    @Test
    void addRawGrapes_RawGrapeIsValid_RawGrapesIsAdded() {
        RawGrapes newGrape = new RawGrapes(200, "test", "test quality");
        rawGrapesService.addRawGrapes(newGrape);

        verify(rawGrapesRepositoryMck).save(newGrape);
    }

    @Test
    void deleteRawGrapes_Everytime_ReturnsOk() {
        rawGrapesService.deleteRawGrapes(123L);

        verify(rawGrapesRepositoryMck).deleteById(123L);
    }

}
