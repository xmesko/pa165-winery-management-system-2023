package cz.fi.muni.service;

import cz.fi.muni.data.model.Field;
import cz.fi.muni.data.repository.FieldRepository;
import cz.fi.muni.exceptions.ResourceNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


import java.util.Date;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class FieldServiceTest {
    @Mock
    private FieldRepository fieldRepository;
    @InjectMocks
    private FieldService fieldService;
    private Field field;

    @BeforeEach
    void setUp() {
        field = new Field(200, new Date(), "test");
        field.setId(123L);
    }

    @Test
    void findById_FieldNotFound_ThrowsNotFound() {
        when(fieldRepository.findById(field.getId())).thenReturn(Optional.empty());

        Exception exception = assertThrows(ResourceNotFoundException.class, () -> {
            fieldService.findById(field.getId());
        });

        assertTrue(exception.getMessage().contains("Field with id: 123 was not found."));
    }

    @Test
    void findById_FieldFound_ReturnsField() {
        when(fieldRepository.findById(field.getId())).thenReturn(Optional.ofNullable(field));

        var actualWine = fieldService.findById(field.getId());

        assertEquals(field, actualWine);
    }

    @Test
    void findAll_FieldsFound_ReturnsFields() {
        when(fieldRepository.findAll()).thenReturn(List.of(field));

        var returnedFields = fieldService.findAll();

        assertEquals(returnedFields, List.of(field));
    }

    @Test
    void editField_IdIsNull_ThrowsIllegalArgumentException() {
        when(fieldRepository.findById(751L)).thenReturn(Optional.empty());

        Exception exception = assertThrows(ResourceNotFoundException.class, () -> {
            fieldService.editField(751L, field);
        });

        assertTrue(exception.getMessage().contains("Field with id: 751 was not found."));
    }

    @Test
    void editField_EditFieldIsValid_FieldIsUpdated() {
        Field updatedField = new Field(300, new Date(), "updated variety");
        updatedField.setId(field.getId());
        when(fieldRepository.findById(field.getId())).thenReturn(Optional.ofNullable(field));

        fieldService.editField(field.getId(), updatedField);

        var actualUpdatedField = fieldService.findById(field.getId());
        assertEquals(updatedField.getHarvestSize(), actualUpdatedField.getHarvestSize());
        assertEquals(updatedField.getVariety(), actualUpdatedField.getVariety());
    }

    @Test
    void harvestField_IdIsNull_ThrowsResourceNotFoundException() {
        when(fieldRepository.findById(751L)).thenReturn(Optional.empty());

        Exception exception = assertThrows(ResourceNotFoundException.class, () -> {
            fieldService.harvestField(751L);
        });

        assertTrue(exception.getMessage().contains("Field with id: 751 was not found."));
    }

    @Test
    void harvestField_FieldIsHarvested_FieldIsHarvested() {
        when(fieldRepository.findById(field.getId())).thenReturn(Optional.ofNullable(field));

        fieldService.harvestField(field.getId());

        verify(fieldRepository).save(field);
        var actualField = fieldService.findById(field.getId());
        assertEquals(actualField.getId(), field.getId());
    }

    @Test
    void addField_FieldIsValid_FieldIsAdded() {
        Field newField = new Field(300, new Date(), "new variety");
        when(fieldRepository.save(newField)).thenReturn(newField);

        fieldService.addField(newField);

        verify(fieldRepository).save(newField);
    }

    @Test
    void deleteField_Everytime_ReturnsOk() {
        fieldService.deleteField(751L);

        verify(fieldRepository).deleteById(751L);
    }
}