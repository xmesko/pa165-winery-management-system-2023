package cz.fi.muni.service;

import cz.fi.muni.data.enums.WineColor;
import cz.fi.muni.data.model.Wine;
import cz.fi.muni.data.repository.WineRepository;
import cz.fi.muni.exceptions.ResourceNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class WineServiceTest {

    @Mock
    private WineRepository wineRepositoryMck;
    private WineService wineService;
    private Wine wine;

    @BeforeEach
    void setUp() {
        wine = new Wine(
                "1",
                WineColor.ORANGE
        );
        wineService = new WineService(wineRepositoryMck);
    }

    @Test
    void findByID() {
        when(wineRepositoryMck.findById(wine.getId())).thenReturn(Optional.ofNullable(wine));
        var returnedWine = wineService.findByID(wine.getId());
        assertEquals(returnedWine, wine);
    }

    @Test
    void findAll() {
        List<Wine> wines = List.of(wine);
        when(wineRepositoryMck.findAll()).thenReturn(wines);
        var returnedWines = wineService.findAll();
        assertEquals(returnedWines, wines);
    }

    @Test
    void registerNewWineType() {
        Long id = 2L;
        var registeredWine = new Wine(
                "2",
                WineColor.YELLOW
        );
        registeredWine.setId(id);
        when(wineRepositoryMck.save(wine)).thenReturn(registeredWine);
        var returnedWineId = wineService.registerNewWineType(wine);
        assertEquals(returnedWineId, id);
    }

    @Test
    void deleteWine() {
        Long id = 3L;
        wine.setId(id);
        when(wineRepositoryMck.findById(wine.getId())).thenReturn(Optional.of(wine));
        assertEquals(wineService.findByID(wine.getId()), wine);
        wineService.deleteWine(wine.getId());
        when(wineRepositoryMck.findById(wine.getId())).thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class, () -> wineService.findByID(wine.getId()));
    }

    @Test
    void editWine() {
        Long id = 4L;
        wine.setId(id);
        when(wineRepositoryMck.findById(wine.getId())).thenReturn(Optional.of(wine));
        assertEquals(wineService.findByID(wine.getId()), wine);
        var editedWine = new Wine(
                "4",
                WineColor.RED
        );
        editedWine.setId(id);
        when(wineRepositoryMck.save(editedWine)).thenReturn(editedWine);
        wineService.editWine(4L,editedWine);
        assertEquals(wineService.findByID(wine.getId()), editedWine);
    }
}