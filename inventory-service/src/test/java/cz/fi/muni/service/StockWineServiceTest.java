package cz.fi.muni.service;

import cz.fi.muni.data.model.StockWine;
import cz.fi.muni.data.repository.StockWineRepository;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;





@ExtendWith(MockitoExtension.class)
final class StockWineServiceTest {
    @Mock
    private StockWineRepository stockWineRepositoryMck;



    private StockWineService stockWineService;
    private StockWine stockWine;


    @BeforeEach
    void setUp() {
        stockWine = new StockWine(
                1L,
                1,
                1,
                1F
        );
        stockWineService = new StockWineService(stockWineRepositoryMck);
    }

    @Test
    void findByID() {
        when(stockWineRepositoryMck.getReferenceById(stockWine.getId())).thenReturn(stockWine);
        var returnedStockWine = stockWineService.findByID(stockWine.getId());
        assertEquals(returnedStockWine, stockWine);
    }

    @Test
    void findAll() {
        when(stockWineRepositoryMck.findAll()).thenReturn(List.of(stockWine));
        var returnedStockWine = stockWineService.findAll();
        assertEquals(returnedStockWine, List.of(stockWine));
    }

    @Test
    void registerNewStockWine() {
        Long id = 1L;
        stockWine.setId(id);
        var registeredStockWine = new StockWine(
                1L,
                1,
                1,
                1F
        );
        registeredStockWine.setId(id);
        when(stockWineRepositoryMck.save(Mockito.any(StockWine.class))).thenAnswer((Answer<StockWine>) invocation -> {
            Object[] args = invocation.getArguments();
            if (args != null && args.length > 0 && args[0] != null) {
                StockWine stockWine = (StockWine) args[0];
                stockWine.setId(id);
                return stockWine;
            }
            return null;
        });

        var returnedStockWine = stockWineService.registerNewStockWine(id,1F);
        assertEquals(returnedStockWine, registeredStockWine.getId());

    }

    @Test
    void editStockWine() {
        Long id = 1L;
        stockWine.setId(id);
        when(stockWineRepositoryMck.getReferenceById(stockWine.getId())).thenReturn(stockWine);
        assertEquals(stockWineService.findByID(stockWine.getId()), stockWine);
        var editedStockWine = new StockWine(
                1L,
                2,
                2,
                2F
        );
        editedStockWine.setId(id);
        when(stockWineRepositoryMck.save(editedStockWine)).thenReturn(editedStockWine);
        stockWineService.editStockWine(editedStockWine);
        assertEquals(stockWineService.findByID(stockWine.getId()), editedStockWine);
    }

    @Test
    void deleteStockWine() {
        Long id = 1L;
        stockWine.setId(id);
        when(stockWineRepositoryMck.getReferenceById(stockWine.getId())).thenReturn(stockWine);
        assertEquals(stockWineService.findByID(stockWine.getId()), stockWine);
        stockWineService.deleteStockWine(id);
        when(stockWineRepositoryMck.getReferenceById(stockWine.getId())).thenReturn(null);
        assertNull(stockWineService.findByID(stockWine.getId()));
    }
}