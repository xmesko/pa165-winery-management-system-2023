package cz.fi.muni.facade;

import cz.fi.muni.data.model.StockWine;

import cz.fi.muni.inventory.model.EditStockWineDto;
import cz.fi.muni.mappers.StockWineMapper;
import cz.fi.muni.service.StockWineService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import static org.mockito.Mockito.when;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class StockWineFacadeTest {

    @Mock
    private StockWineService stockWineServiceMck;

    @Mock
    private StockWineMapper stockWineMapperMck;

    @InjectMocks
    private StockWineFacade stockWineFacade;

    private final Long stockWineId = 1L;
    private final StockWine stockWine = new StockWine();
    private final cz.fi.muni.inventory.model.StockWineDto stockWineDto = new cz.fi.muni.inventory.model.StockWineDto();

    @BeforeEach
    void setUp() {
        stockWine.setId(stockWineId);
        stockWineDto.setId(String.valueOf(stockWineId));
    }



    @Test
    void getAllStockWines() {
        List<StockWine> stockWines = List.of(stockWine);
        List<cz.fi.muni.inventory.model.StockWineDto> stockWineDtos = List.of(stockWineDto);

        when(stockWineServiceMck.findAll()).thenReturn(stockWines);
        when(stockWineMapperMck.mapToOutList(stockWines)).thenReturn(stockWineDtos);

        var returnedStockWines = stockWineFacade.getAllStockWines();
        assertEquals(stockWineDtos, returnedStockWines);
    }

    @Test
    void getStockWineById() {
        when(stockWineServiceMck.findByID(stockWineId)).thenReturn(stockWine);
        when(stockWineMapperMck.mapToOutWine(stockWine)).thenReturn(stockWineDto);

        var returnedStockWine = stockWineFacade.getStockWineById(stockWineId);
        assertEquals(stockWineDto, returnedStockWine);
    }

}