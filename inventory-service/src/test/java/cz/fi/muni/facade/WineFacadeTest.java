package cz.fi.muni.facade;

import cz.fi.muni.data.enums.WineColor;

import cz.fi.muni.data.model.Wine;

import cz.fi.muni.mappers.WineMapper;
import cz.fi.muni.service.StockWineService;
import cz.fi.muni.service.WineService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class WineFacadeTest {

    @Mock
    private WineService wineServiceMck;

    @Mock
    private WineMapper wineMapperMck;

    @Mock
    private StockWineService stockWineServiceMck;

    @InjectMocks
    private WineFacade wineFacade;

    private final Long wineId = 1L;
    private final Wine wine = new Wine(
                "1",
                WineColor.ORANGE
                );
    private final cz.fi.muni.inventory.model.WineDto wineDto = new cz.fi.muni.inventory.model.WineDto();
    private final cz.fi.muni.inventory.model.AddWineDto addWineDto = new cz.fi.muni.inventory.model.AddWineDto();
    @BeforeEach
    void setUp() {
        wine.setId(wineId);
        wineDto.setColor(cz.fi.muni.inventory.model.WineDto.ColorEnum.ORANGE);
        wineDto.setVariety("1");
        addWineDto.setColor(cz.fi.muni.inventory.model.AddWineDto.ColorEnum.ORANGE);
        addWineDto.setVariety("1");
    }

    @Test
    void getAllWines() {
        List<Wine> wines = List.of(wine);
        List<cz.fi.muni.inventory.model.WineDto> wineDtos = List.of(wineDto);

        when(wineServiceMck.findAll()).thenReturn(wines);
        when(wineMapperMck.mapToOutList(wines)).thenReturn(List.of(wineDto));
        var returnedWines = wineFacade.getAllWines();
        assertEquals(returnedWines, wineDtos);
    }

    @Test
    void getWineById() {
        when(wineServiceMck.findByID(wineId)).thenReturn(wine);
        when(wineMapperMck.mapToOutWine(wine)).thenReturn(wineDto);
        var returnedWine = wineFacade.getWineById(wineId);
        assertEquals(returnedWine, wineDto);
    }

    @Test
    void registerWine() {
        when(wineServiceMck.registerNewWineType(wine)).thenReturn(wine.getId());
        when(wineMapperMck.mapToInWine(addWineDto)).thenReturn(wine);
        var returnedId = wineFacade.registerWine(addWineDto);
        assertEquals(returnedId, wine.getId());
    }

}