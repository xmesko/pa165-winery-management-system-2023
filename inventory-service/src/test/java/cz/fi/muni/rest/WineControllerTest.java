package cz.fi.muni.rest;

import cz.fi.muni.data.repository.OrderRepository;
import cz.fi.muni.data.repository.StockWineRepository;
import cz.fi.muni.data.repository.WineRepository;
import cz.fi.muni.facade.WineFacade;
import cz.fi.muni.service.WineService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = OrderController.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc(addFilters = false)
class WineControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private WineFacade wineFacade;

    @MockBean
    private WineService wineService;

    @MockBean
    private StockWineRepository stockWineRepository;
    @MockBean
    private WineRepository wineRepository;
    @MockBean
    private OrderRepository orderRepository;

    @Test
    void createWine() throws Exception {
        mockMvc.perform(post("/wine")
                .contentType("application/json")
                .content("{\"name\":\"test\",\"description\":\"test\",\"year\":2020,\"alcoholVolume\":0.1,\"stock\":1,\"price\":1.0,\"type\":\"RED\"}"))
                .andExpect(status().isCreated());
        verify(wineFacade).registerWine(any());
    }

    @Test
    void deleteWine() throws Exception {
        mockMvc.perform(delete("/wine/1"))
                .andExpect(status().is2xxSuccessful());
        verify(wineFacade).deleteWine(any());
    }

    @Test
    void editWine() throws Exception {
        mockMvc.perform(patch("/wine/1")
                .contentType("application/json")
                .content("{\"name\":\"test\",\"description\":\"test\",\"year\":2020,\"alcoholVolume\":0.1,\"stock\":1,\"price\":1.0,\"type\":\"RED\"}"))
                .andExpect(status().isOk());
        verify(wineFacade).editWine(any(),any());
    }

    @Test
    void getAllWines() throws Exception {
        mockMvc.perform(get("/wines"))
                .andExpect(status().isOk());
        verify(wineFacade).getAllWines();
    }

    @Test
    void getWine() throws Exception {
        mockMvc.perform(get("/wine/1"))
                .andExpect(status().isOk());
        verify(wineFacade).getWineById(1L);
    }
}