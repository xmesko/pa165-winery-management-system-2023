package cz.fi.muni.rest;

import cz.fi.muni.data.repository.OrderRepository;
import cz.fi.muni.data.repository.StockWineRepository;
import cz.fi.muni.data.repository.WineRepository;
import cz.fi.muni.facade.StockWineFacade;
import cz.fi.muni.service.StockWineService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(MockitoExtension.class)
@WebMvcTest(controllers = StockWineController.class)
@AutoConfigureMockMvc(addFilters = false)
class StockWineControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StockWineFacade stockWineFacade;

    @MockBean
    private StockWineService stockWineService;

    @MockBean
    private StockWineRepository stockWineRepository;
    @MockBean
    private WineRepository wineRepository;
    @MockBean
    private OrderRepository orderRepository;

    @BeforeEach
    void setUp() {
    }

    @Test
    void editStockWine() throws Exception {
        mockMvc.perform(patch("/stockWine")
                .contentType("application/json")
                .content("{\"id\":1,\"wine\":{\"id\":1,\"name\":\"Wine1\",\"year\":2019,\"batch\":1,\"country\":\"Czech Republic\",\"region\":\"Moravia\",\"color\":\"RED\",\"style\":\"DRY\",\"volume\":0.75,\"price\":100.0,\"currency\":\"CZK\"},\"amount\":10,\"reserved\":0,\"available\":10}"))
                .andExpect(status().isOk());
        verify(stockWineFacade).editStockWine(any());
    }

    @Test
    void getAllStockWines() throws Exception {
        mockMvc.perform(get("/stockWines"))
                .andExpect(status().isOk());
        verify(stockWineFacade).getAllStockWines();
    }

    @Test
    void getStockInfo() throws Exception {
        mockMvc.perform(get("/stockWine/1"))
                .andExpect(status().isOk());
        verify(stockWineFacade).getStockWineById(any());
    }
}