package cz.fi.muni.rest;

import cz.fi.muni.data.repository.OrderRepository;
import cz.fi.muni.data.repository.StockWineRepository;
import cz.fi.muni.data.repository.WineRepository;
import cz.fi.muni.facade.OrderFacade;
import cz.fi.muni.inventory.model.Address;
import cz.fi.muni.inventory.model.BaseOrderDto;
import cz.fi.muni.service.OrderService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = OrderController.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc(addFilters = false)
class OrderControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private OrderFacade orderFacade;

    @MockBean
    private OrderService orderService;

    @MockBean
    private StockWineRepository stockWineRepository;
    @MockBean
    private WineRepository wineRepository;
    @MockBean
    private OrderRepository orderRepository;

    Address address = new Address();


    @BeforeEach
    void setUp() {
        address.setCity("Brno");
        address.setCountry("Czech Republic");
        address.setStreet("Botanicka");
        address.setZipCode("12345");
        //when(orderService.registerNewOrder();
    }

    @Test
    void buyWines() throws Exception {
        mockMvc.perform(post("/order")
                .contentType("application/json")
                .content("{\"address\":{\"street\":\"Botanicka\",\"city\":\"Brno\",\"zipCode\":\"12345\",\"country\":\"Czech Republic\"},\"wines\":[{\"id\":1,\"name\":\"Wine1\",\"year\":2010,\"batch\":1,\"color\":\"RED\",\"volume\":0.75,\"price\":100.0,\"stock\":10,\"reserved\":0,\"sold\":0,\"available\":10,\"description\":\"Wine1 description\"},{\"id\":2,\"name\":\"Wine2\",\"year\":2011,\"batch\":2,\"color\":\"WHITE\",\"volume\":0.75,\"price\":200.0,\"stock\":20,\"reserved\":0,\"sold\":0,\"available\":20,\"description\":\"Wine2 description\"}],\"amounts\":[1,2]}"))
                .andExpect(status().isOk());
        verify(orderFacade).createOrder(any(BaseOrderDto.class));
    }

    @Test
    void getAllOrders() throws Exception {
        mockMvc.perform(get("/orders"))
                .andExpect(status().isOk());
        verify(orderFacade).getAllOrders(null);
    }

    @Test
    void getOrder() throws Exception {
        mockMvc.perform(get("/order/1"))
                .andExpect(status().isOk());
        verify(orderFacade).getOrder(1L);
    }
}