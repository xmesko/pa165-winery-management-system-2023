package cz.fi.muni.mappers;

import cz.fi.muni.data.model.Order;
import cz.fi.muni.data.model.Wine;
import cz.fi.muni.inventory.model.OrderDto;
import cz.fi.muni.inventory.model.BaseOrderDto;
import cz.fi.muni.inventory.model.OrderItemDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Component
@Mapper(componentModel = "spring")
public interface OrderMapper {

    @Mapping(target = "deliveryAddress.city", source= "city")
    @Mapping(target = "deliveryAddress.street", source= "street")
    @Mapping(target = "deliveryAddress.zipCode", source= "zipCode")
    @Mapping(target = "deliveryAddress.country", source= "country")
    @Mapping(target = "telephoneNumber", source= "telephoneNumber")
    @Mapping(target = "userId", source= "userAccountId")
    @Mapping(target = "orderItems", source = "boughtWines", qualifiedByName = "mapMapToList")
    OrderDto mapToOutOrder(Order order);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "city", source= "deliveryAddress.city")
    @Mapping(target = "street", source= "deliveryAddress.street")
    @Mapping(target = "zipCode", source= "deliveryAddress.zipCode")
    @Mapping(target = "country", source= "deliveryAddress.country")
    @Mapping(target = "telephoneNumber", source= "telephoneNumber")
    @Mapping(target = "userAccountId", source= "userId")
    Order mapToInOrder(BaseOrderDto order);

    default Map<Long, Integer> mapListToMap(List<OrderItemDto> boughtWines) {
        Map<Long, Integer> map = new HashMap<>();
        boughtWines.forEach(wine -> map.put(Long.parseLong(wine.getId()), wine.getAmount()));
        return map;
    }

    @Named("mapMapToList")
    default List<OrderItemDto> mapToTargetObject(Map<Long, Integer> boughtWines) {
        List<OrderItemDto> orderItems = new ArrayList<>();
        boughtWines.forEach((key, value) -> {
            OrderItemDto orderItemDto = new OrderItemDto();
            orderItemDto.setId(String.valueOf(key));
            orderItemDto.setAmount(value);
            orderItems.add(orderItemDto);
        });
        return orderItems;
    }

    OrderItemDto mapToOutOrderItem(Wine wine);

    List<OrderDto> mapToOutList(List<Order> wines);

}
