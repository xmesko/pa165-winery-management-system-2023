package cz.fi.muni.mappers;
import cz.fi.muni.data.enums.WineColor;
import cz.fi.muni.data.model.Wine;
import cz.fi.muni.inventory.model.AddWineDto;
import cz.fi.muni.inventory.model.EditWineDto;
import cz.fi.muni.inventory.model.WineDto;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;
import org.openapitools.jackson.nullable.JsonNullable;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Function;


@Component
@Mapper(componentModel = "spring")
public interface WineMapper {

    Wine mapToInWine(WineDto wine);

    @Mapping(target = "id", ignore = true)
    Wine mapToInWine(EditWineDto wine);

    @Mapping(target = "id", ignore = true)
    Wine mapToInWine(AddWineDto wine);
    WineDto mapToOutWine(Wine wine);

    List<WineDto> mapToOutList(List<Wine> wines);

}
