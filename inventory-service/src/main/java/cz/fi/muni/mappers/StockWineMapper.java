package cz.fi.muni.mappers;

import cz.fi.muni.data.model.StockWine;
import cz.fi.muni.data.model.Wine;
import cz.fi.muni.inventory.model.EditStockWineDto;
import cz.fi.muni.inventory.model.StockWineDto;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(componentModel = "spring")
public interface StockWineMapper {

    StockWine mapToInWine(StockWineDto wine);

    StockWine mapToInWine(EditStockWineDto wine);

   StockWineDto mapToOutWine(StockWine wine);

    List<StockWineDto> mapToOutList(List<StockWine> wines);
}
