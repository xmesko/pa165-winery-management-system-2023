package cz.fi.muni.data.repository;

import cz.fi.muni.data.model.Wine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface WineRepository extends JpaRepository<Wine, Long> {

}
