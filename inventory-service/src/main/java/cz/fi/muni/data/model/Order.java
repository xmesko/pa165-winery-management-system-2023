package cz.fi.muni.data.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;

import java.util.Map;
import java.util.Objects;

@Entity
@Table(name = "wines_order")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @NotNull
    @Column(name = "account_id")
    private Long userAccountId;

    @Column(name = "telephone_number")
    private String telephoneNumber;

    @NotNull
    @Column(name = "address_street")
    private String street;

    @NotNull
    @Column(name = "address_city")
    private String city;

    @NotNull
    @Column(name = "address_zip_code")
    private String zipCode;

    @NotNull
    @Column(name = "address_country")
    private String country;

    @ElementCollection
    @CollectionTable(name = "wines_order_mappings",
            joinColumns = {@JoinColumn(name = "wines_order_id", referencedColumnName = "id")})
    @MapKeyColumn(name = "wine_id")
    @Column(name = "amount")
    private Map<Long, Integer> boughtWines;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserAccountId() {
        return userAccountId;
    }

    public void setUserAccountId(Long userAccountId) {
        this.userAccountId = userAccountId;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order order)) return false;
        return getId().equals(order.getId()) && getUserAccountId().equals(order.getUserAccountId()) && Objects.equals(getTelephoneNumber(), order.getTelephoneNumber()) && getStreet().equals(order.getStreet()) && getCity().equals(order.getCity()) && getZipCode().equals(order.getZipCode()) && getCountry().equals(order.getCountry());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    public Map<Long, Integer> getBoughtWines() {
        return boughtWines;
    }

    public void setBoughtWines(Map<Long, Integer> boughtWines) {
        this.boughtWines = boughtWines;
    }
}
