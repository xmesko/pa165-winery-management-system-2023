package cz.fi.muni.data.repository;

import cz.fi.muni.data.model.StockWine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface StockWineRepository extends JpaRepository<StockWine, Long> {

    @Query("SELECT s.bottlesAvailable FROM StockWine s WHERE s.id = :id")
    Integer winesInStockById(Long id);
}
