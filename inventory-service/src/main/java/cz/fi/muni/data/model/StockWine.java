package cz.fi.muni.data.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;

import java.util.Objects;

@Entity
@Table(name = "stock_wine")
public class StockWine {
    @Id
    private Long id;
    @NotNull
    @Column(name = "bottles_sold")
    private Integer bottlesSold;

    @NotNull
    @Column(name = "bottles_available")
    private Integer bottlesAvailable;

    @NotNull
    @Column(name = "price")
    private Float price;

    @Column(name = "profit")
    private Float profit;

    public StockWine() {
    }

    public StockWine(Long id, Integer bottlesSold, Integer bottlesAvailable, Float price) {
        this.id = id;
        this.bottlesSold = bottlesSold;
        this.bottlesAvailable = bottlesAvailable;
        this.price = price;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getBottlesSold() {
        return bottlesSold;
    }

    public void setBottlesSold(Integer bottlesSold) {
        this.bottlesSold = bottlesSold;
    }

    public Integer getBottlesAvailable() {
        return bottlesAvailable;
    }

    public void setBottlesAvailable(Integer bottlesAvailable) {
        this.bottlesAvailable = bottlesAvailable;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getProfit() {
        return profit;
    }

    public void setProfit(Float profit) {
        this.profit = profit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StockWine stockWine)) return false;
        return getId().equals(stockWine.getId()) && getBottlesSold().equals(stockWine.getBottlesSold()) && getBottlesAvailable().equals(stockWine.getBottlesAvailable()) && getPrice().equals(stockWine.getPrice()) && Objects.equals(getProfit(), stockWine.getProfit());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getBottlesSold(), getBottlesAvailable(), getPrice(), getProfit());
    }
}