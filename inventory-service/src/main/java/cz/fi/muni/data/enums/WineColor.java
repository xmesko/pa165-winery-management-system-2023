package cz.fi.muni.data.enums;

public enum WineColor {
    RED, WHITE, ROSE, YELLOW, ORANGE
}
