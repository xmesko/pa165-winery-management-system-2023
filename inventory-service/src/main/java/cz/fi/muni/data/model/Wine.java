package cz.fi.muni.data.model;

import cz.fi.muni.data.enums.WineColor;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;

import java.util.Objects;

@Entity
@Table(name = "wine")
public class Wine {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    Long id;

    @NotNull
    @Column(name = "variety")
    String variety;

    @NotNull
    @Column(name = "color")
    WineColor color;

    // TODO: add limits
    @Column(name = "year")
    Integer year;

    @Column(name = "sweetness")
    Float sweetness;

    @Column(name = "alcohol_content")
    Float alcoholContent;

    @Column(name = "description", columnDefinition = "TEXT")
    String description;


    public Wine() {}

    public Wine(String variety, WineColor color) {
        this.variety = variety;
        this.color = color;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Wine wine)) return false;
        return getId().equals(wine.getId()) && getVariety().equals(wine.getVariety()) && getColor() == wine.getColor() && Objects.equals(getYear(), wine.getYear());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    public Long getId() {
        return id;
    }

    public String getVariety() {
        return variety;
    }

    public WineColor getColor() {
        return color;
    }

    public Integer getYear() {
        return year;
    }

    public Float getSweetness() {
        return sweetness;
    }

    public Float getAlcoholContent() {
        return alcoholContent;
    }

    public String getDescription() {
        return description;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setVariety(String variety) {
        this.variety = variety;
    }

    public void setColor(WineColor color) {
        this.color = color;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public void setSweetness(Float sweetness) {
        this.sweetness = sweetness;
    }

    public void setAlcoholContent(Float alcoholContent) {
        this.alcoholContent = alcoholContent;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
