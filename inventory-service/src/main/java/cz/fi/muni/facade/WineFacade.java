package cz.fi.muni.facade;

import cz.fi.muni.inventory.model.AddWineDto;
import cz.fi.muni.inventory.model.EditWineDto;
import cz.fi.muni.inventory.model.WineDto;
import cz.fi.muni.mappers.WineMapper;
import cz.fi.muni.service.IStockWineService;
import cz.fi.muni.service.IWineService;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Transactional
public class WineFacade implements IWineFacade {
    private static final Logger log = LoggerFactory.getLogger(WineFacade.class);

    private final IWineService wineService;
    private final IStockWineService stockWineService;

    private final WineMapper wineMapper;
    @Autowired
    public WineFacade(IWineService wineService,
                      IStockWineService stockWineService,
                      WineMapper wineMapper) {

        this.wineService = wineService;
        this.wineMapper = wineMapper;
         this.stockWineService = stockWineService;
    }

    @Override
    public List<WineDto> getAllWines() {
        return wineMapper.mapToOutList(wineService.findAll());
    }

    @Override
    public WineDto getWineById(Long id) {

        return wineMapper.mapToOutWine(wineService.findByID(id));
    }

    @Override
    public Long registerWine(AddWineDto wine) {
        log.debug("Registering new wine type: " + wine);
        Long newWineId = wineService.registerNewWineType(wineMapper.mapToInWine(wine));
        stockWineService.registerNewStockWine(newWineId, wine.getPrice());
        return newWineId;
    }

    @Override
    public void deleteWine(Long id) {
        log.debug("Deleting wine type: " + id);
        Integer bottlesLeft = stockWineService.findByID(id).getBottlesAvailable();
        if (bottlesLeft > 0) {
            throw new IllegalArgumentException("Cannot delete wine type with id " + id + " because there are still " + bottlesLeft + " bottles left.");
        }
        stockWineService.deleteStockWine(id);
        wineService.deleteWine(id);
    }

    @Override
    public void editWine(Long id, EditWineDto editWine) {
        wineService.editWine(id, wineMapper.mapToInWine(editWine));
    }
}
