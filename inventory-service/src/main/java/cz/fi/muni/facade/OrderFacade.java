package cz.fi.muni.facade;

import cz.fi.muni.data.model.Order;
import cz.fi.muni.exceptions.ResourcesNotSufficient;
import cz.fi.muni.inventory.model.OrderDto;
import cz.fi.muni.inventory.model.BaseOrderDto;
import cz.fi.muni.mappers.OrderMapper;
import cz.fi.muni.service.IOrderService;
import cz.fi.muni.service.IStockWineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class OrderFacade implements IOrderFacade {

    private final IOrderService orderService;
    private final IStockWineService stockWineService;
    private final OrderMapper orderMapper;

    @Autowired
    public OrderFacade(
            IOrderService orderService,
            IStockWineService stockWineService,
            OrderMapper orderMapper) {
        this.orderService = orderService;
        this.stockWineService = stockWineService;
        this.orderMapper = orderMapper;
    }

    @Override
    public void createOrder(BaseOrderDto order) {
        Map<Long, Integer> boughtWines = orderMapper.mapListToMap(order.getOrderItems());
        boolean isSufficientStock = stockWineService.sufficientStock(boughtWines);
        if (!isSufficientStock) {
            throw new ResourcesNotSufficient(
                    "Not enough bottles of wine in stock for selected wines: "
                            + order.getOrderItems());
        }
        Order newOrder = orderMapper.mapToInOrder(order);
        newOrder.setBoughtWines(boughtWines);
        orderService.registerNewOrder(newOrder);
        stockWineService.sellBottles(boughtWines);
    }

    @Override
    public OrderDto getOrder(Long id) {
        return orderMapper.mapToOutOrder(orderService.findByID(id));
    }

    @Override
    public List<OrderDto> getAllOrders(String userId) {
        return userId != null
                ? orderMapper.mapToOutList(orderService.findAllByUserId(Long.parseLong(userId)))
                : orderMapper.mapToOutList(orderService.findAll());
    }
}
