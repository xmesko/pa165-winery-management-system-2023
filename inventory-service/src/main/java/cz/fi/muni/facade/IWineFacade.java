package cz.fi.muni.facade;

import cz.fi.muni.inventory.model.AddWineDto;
import cz.fi.muni.inventory.model.EditWineDto;
import cz.fi.muni.inventory.model.WineDto;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;


@Service
public interface IWineFacade {
    WineDto getWineById(Long id);
    List<WineDto> getAllWines();

    Long registerWine(AddWineDto wine);

    void deleteWine(Long id);

    void editWine(Long id, EditWineDto editWine);

}
