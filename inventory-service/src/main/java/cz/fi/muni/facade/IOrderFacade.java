package cz.fi.muni.facade;

import cz.fi.muni.inventory.model.OrderDto;
import cz.fi.muni.inventory.model.BaseOrderDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IOrderFacade {
    void createOrder(BaseOrderDto order);
    OrderDto getOrder(Long id);
    List<OrderDto> getAllOrders(String userId);
}
