package cz.fi.muni.facade;

import cz.fi.muni.inventory.model.EditStockWineDto;
import cz.fi.muni.inventory.model.StockWineDto;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public interface IStockWineFacade {
    List<StockWineDto> getAllStockWines();

    StockWineDto getStockWineById(Long id);
    void editStockWine(EditStockWineDto editStockWine);
}
