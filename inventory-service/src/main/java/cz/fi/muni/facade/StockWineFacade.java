package cz.fi.muni.facade;

import cz.fi.muni.inventory.model.EditStockWineDto;
import cz.fi.muni.inventory.model.StockWineDto;
import cz.fi.muni.mappers.StockWineMapper;
import cz.fi.muni.service.IStockWineService;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Transactional
public class StockWineFacade implements IStockWineFacade {

    private final IStockWineService stockWineService;
    private final StockWineMapper stockWineMapper;
    @Autowired
    public StockWineFacade(IStockWineService stockWineService,
                           StockWineMapper stockWineMapper) {
        this.stockWineService = stockWineService;
        this.stockWineMapper = stockWineMapper;
    }

    @Override
    public List<StockWineDto> getAllStockWines() {
        return stockWineMapper.mapToOutList(stockWineService.findAll());
    }

    @Override
    public StockWineDto getStockWineById(Long id) {
        return stockWineMapper.mapToOutWine(stockWineService.findByID(id));
    }

    @Override
    public void editStockWine(EditStockWineDto editStockWine) {
        stockWineService.editStockWine(stockWineMapper.mapToInWine(editStockWine));
    }
}
