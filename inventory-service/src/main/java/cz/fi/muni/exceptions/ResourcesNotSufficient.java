package cz.fi.muni.exceptions;

public class ResourcesNotSufficient extends RuntimeException {
    public ResourcesNotSufficient() {
    }

    public ResourcesNotSufficient(String message) {
        super(message);
    }

    public ResourcesNotSufficient(String message, Throwable cause) {
        super(message, cause);
    }
}
