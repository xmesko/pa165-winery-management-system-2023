package cz.fi.muni.config;


import io.swagger.v3.oas.models.security.*;
import org.springdoc.core.customizers.OpenApiCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.resource.OAuth2ResourceServerConfigurer;

@Configuration
@EnableWebSecurity
@EnableWebMvc
public class SecurityConfiguration {


    @Bean
    public OpenApiCustomizer openAPICustomizer() {
        return openApi -> {
            openApi.getComponents()
                    .addSecuritySchemes("Bearer",
                            new SecurityScheme()
                                    .type(SecurityScheme.Type.HTTP)
                                    .scheme("bearer")
                                    .description("provide a valid access token")
                    );
            var managerScope = new SecurityRequirement().addList("Bearer", "test_write");
            var userScope = new SecurityRequirement().addList("Bearer", "test_read");


            openApi.getPaths().get("/wine").getPost().addSecurityItem(managerScope);
            openApi.getPaths().get("/wine/{id}").getGet().addSecurityItem(userScope);
            openApi.getPaths().get("/wine/{id}").getPatch().addSecurityItem(managerScope);
            openApi.getPaths().get("/wine/{id}").getDelete().addSecurityItem(managerScope);
            openApi.getPaths().get("/wines").getGet().addSecurityItem(userScope);

            openApi.getPaths().get("/stockWine").getPatch().addSecurityItem(managerScope);
            openApi.getPaths().get("/stockWine/{id}").getGet().addSecurityItem(managerScope);
            openApi.getPaths().get("/stockWines").getGet().addSecurityItem(managerScope);

            openApi.getPaths().get("/order").getPost().addSecurityItem(userScope);
            openApi.getPaths().get("/orders").getGet().addSecurityItem(managerScope);
            openApi.getPaths().get("/order/{id}").getGet().addSecurityItem(userScope);
        };
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.authorizeHttpRequests(x -> x
                        .requestMatchers("/swagger-ui/**", "/v3/api-docs/**", "/").permitAll()

                        .requestMatchers("/wine").hasAuthority("SCOPE_test_write")
                        .requestMatchers(HttpMethod.GET, "/wine/**").hasAuthority("SCOPE_test_read")
                        .requestMatchers(HttpMethod.PATCH, "/wine/**").hasAuthority("SCOPE_test_write")
                        .requestMatchers(HttpMethod.DELETE, "/wine/**").hasAuthority("SCOPE_test_write")
                        .requestMatchers("/wines").hasAuthority("SCOPE_test_read")

                        .requestMatchers("/stockWine").hasAuthority("SCOPE_test_write")
                        .requestMatchers("/stockWine/**").hasAuthority("SCOPE_test_write")
                        .requestMatchers("/stockWines").hasAuthority("SCOPE_test_write")

                        .requestMatchers("/order").hasAuthority("SCOPE_test_read")
                        .requestMatchers("/order/**").hasAuthority("SCOPE_test_read")
                        .requestMatchers("/orders").hasAuthority("SCOPE_test_write")
                        .anyRequest().permitAll()
                )
                .oauth2ResourceServer(OAuth2ResourceServerConfigurer::opaqueToken)
        ;
        return http.build();
    }

}
