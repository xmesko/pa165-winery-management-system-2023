package cz.fi.muni.service;

import cz.fi.muni.data.model.Order;
import cz.fi.muni.data.repository.OrderRepository;
import cz.fi.muni.exceptions.ResourceNotFoundException;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Transactional
public class OrderService implements IOrderService {

    private final OrderRepository orderRepository;
    private static final Logger log = LoggerFactory.getLogger(OrderService.class);

    @Autowired
    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }
    @Override
    public List<Order> findAll() {
        return orderRepository.findAll();
    }

    @Override
    public List<Order> findAllByUserId(Long userId) {
        return orderRepository.findByUserId(userId);
    }

    @Override
    public Order findByID(Long id) {

        return orderRepository
                .findById(id)
                .map(order -> {
                    log.info("Found order: " + order);
                    return order;
                })
                .orElseThrow(() ->
                        new ResourceNotFoundException(
                                "Order with id: " + id + " was not found."
                        ));
    }

    @Override
    public Long registerNewOrder(Order order) {
        log.info("Creating new order: " + order);
        return orderRepository.save(order).getId();
    }
}
