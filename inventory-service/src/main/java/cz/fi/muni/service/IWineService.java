package cz.fi.muni.service;

import cz.fi.muni.data.model.Wine;
// TODO: DTO


import java.util.List;

public interface IWineService {
    Wine findByID(Long id);

    List<Wine> findAll();

    Long registerNewWineType(Wine wine);

    void deleteWine(Long id);

    void editWine(Long id, Wine editWine);
}
