package cz.fi.muni.service;

import cz.fi.muni.data.model.Order;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IOrderService {
    List<Order> findAll();
    List<Order> findAllByUserId(Long userId);
    Order findByID(Long id);
    Long registerNewOrder(Order order);
}
