package cz.fi.muni.service;

import cz.fi.muni.data.model.Wine;
import cz.fi.muni.data.repository.WineRepository;
import cz.fi.muni.exceptions.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class WineService implements IWineService {
    private static final Logger log = LoggerFactory.getLogger(WineService.class);

    private final WineRepository wineRepository;

    @Autowired
    public WineService(WineRepository wineRepository) {
        this.wineRepository = wineRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public Wine findByID(Long id) {
        return wineRepository
                .findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Wine with id: " + id + " was not found."));
    }

    @Transactional(readOnly = true)
    public List<Wine> findAll() {
        return wineRepository.findAll();
    }

    @Override
    public Long registerNewWineType(Wine wine) {
        log.error("Registering new wine type: " + wine);
        Wine newWine = wineRepository.save(wine);
        return newWine.getId();
    }

    @Override
    public void deleteWine(Long id) {
        wineRepository.deleteById(id);
    }

    @Override
    public void editWine(Long id, Wine editWine) {
        Wine wine = wineRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Wine with id: " + id + " was not found."));
        wine.setVariety(editWine.getVariety());
        wine.setYear(editWine.getYear());
        wine.setAlcoholContent(editWine.getAlcoholContent());
        wine.setColor(editWine.getColor());
        wine.setDescription(editWine.getDescription());
        editWine.setSweetness(editWine.getSweetness());
        wineRepository.save(wine);
    }

}
