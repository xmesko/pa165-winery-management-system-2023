package cz.fi.muni.service;

import cz.fi.muni.data.model.StockWine;
import cz.fi.muni.data.repository.StockWineRepository;
import cz.fi.muni.exceptions.ResourceNotFoundException;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
@Transactional
public class StockWineService implements IStockWineService {

    private final StockWineRepository stockWineRepository;
    @Autowired
    public StockWineService(StockWineRepository stockWineRepository) {
        this.stockWineRepository = stockWineRepository;
    }

    @Override
    public StockWine findByID(Long id) {
        return stockWineRepository.getReferenceById(id);
    }

    @Override
    public List<StockWine> findAll() {
        return stockWineRepository.findAll();
    }

    @Override
    public boolean sufficientStock(Map<Long, Integer> wines) {
        boolean isStockSufficient = wines.keySet().stream().map(id ->
            stockWineRepository.winesInStockById(id) >= wines.get(id)
        ).reduce(true, (a, b) -> a && b);
        return isStockSufficient;
    }

    @Override
    public Long registerNewStockWine(Long id, Float price) {
        StockWine sw = new StockWine();
        sw.setId(id);
        sw.setPrice(price);
        sw.setBottlesAvailable(0);
        sw.setBottlesSold(0);
        sw.setProfit(0.0f);
        return stockWineRepository.save(sw).getId();
    }

    @Override
    @Transactional
    public void editStockWine(StockWine wine) {
        StockWine current = findByID(wine.getId());
        if (current == null) {
            throw new ResourceNotFoundException("StockWine with id " + wine.getId() + " not found");
        }
        current.setPrice(wine.getPrice());
        current.setBottlesAvailable(wine.getBottlesAvailable());
        current.setBottlesSold(wine.getBottlesSold());
        current.setProfit(wine.getProfit());
        stockWineRepository.save(current);
    }

    @Override
    public void sellBottles(Map<Long, Integer> bottles) {
        bottles.keySet().forEach(id -> {
            StockWine sw = findByID(id);
            if (sw == null) {
                throw new ResourceNotFoundException("StockWine with id " + id + " not found");
            }
            sw.setProfit(sw.getProfit() + bottles.get(id) * sw.getPrice());
            sw.setBottlesAvailable(sw.getBottlesAvailable() - bottles.get(id));
            sw.setBottlesSold(sw.getBottlesSold() + bottles.get(id));
            stockWineRepository.save(sw);
        });
    }


    @Override
    public void deleteStockWine(Long id) {
        stockWineRepository.deleteById(id);
    }

}
