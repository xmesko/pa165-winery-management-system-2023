package cz.fi.muni.service;

import cz.fi.muni.data.model.StockWine;

import java.util.List;
import java.util.Map;

public interface IStockWineService {
    StockWine findByID(Long id);
    List<StockWine> findAll();
    boolean sufficientStock(Map<Long, Integer> wines);
    Long registerNewStockWine(Long id, Float price);
    void editStockWine(StockWine wine);
    void sellBottles(Map<Long, Integer> bottles);
    void deleteStockWine(Long id);
}
