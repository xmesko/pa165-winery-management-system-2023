package cz.fi.muni.rest;

import cz.fi.muni.facade.IWineFacade;
import cz.fi.muni.inventory.api.WineApiDelegate;
import cz.fi.muni.inventory.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class WineController implements WineApiDelegate {
    private static final Logger log = LoggerFactory.getLogger(WineController.class);

    private final IWineFacade wineFacade;

    @Autowired
    public WineController(IWineFacade wineFacade) {
        this.wineFacade = wineFacade;
    }

    @Override
    public ResponseEntity<NewWineResponse> createWine(AddWineDto newWine) {
        if (newWine == null) {
            log.info("No wine to be created provided.");
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        Long newId = wineFacade.registerWine(newWine);
        NewWineResponse newWineResponse = new NewWineResponse() {
            {
                message(newId.toString());
            }
        };
        return new ResponseEntity<>(newWineResponse, HttpStatus.CREATED);

    }

    @Override
    public ResponseEntity<Void> deleteWine(String id) {
        if (id == null) {
            log.info("Provided wine identifier is null.");
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        Long longId = Long.parseLong(id);
        wineFacade.deleteWine(longId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Override
    public ResponseEntity<Void> editWine(String id, EditWineDto editWine) {
        if (editWine == null) {
            log.debug("No parameters to edit provided.");
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        Long longId = Long.parseLong(id);
        wineFacade.editWine(longId, editWine);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<WineDto>> getAllWines() {
        // retrieve all wines from DB
        log.info("getAllWines");
        return new ResponseEntity<>(wineFacade.getAllWines(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<WineDto> getWine(String id) {
        if (id == null) {
            log.info("Provided wine identifier is null.");
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        Long longId = Long.parseLong(id);
        return new ResponseEntity<>(wineFacade.getWineById(longId), HttpStatus.OK);
    }


}
