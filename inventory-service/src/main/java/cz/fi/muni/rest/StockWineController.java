package cz.fi.muni.rest;

import cz.fi.muni.facade.IStockWineFacade;
import cz.fi.muni.inventory.api.StockWineApiDelegate;
import cz.fi.muni.inventory.model.EditStockWineDto;
import cz.fi.muni.inventory.model.StockWineDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class StockWineController implements StockWineApiDelegate {
    private static final Logger log = LoggerFactory.getLogger(StockWineController.class);
    private final IStockWineFacade stockWineFacade;
    @Autowired
    public StockWineController(IStockWineFacade stockWineFacade) {
        this.stockWineFacade = stockWineFacade;
    }

    @Override
    public ResponseEntity<Void> editStockWine(EditStockWineDto editStockWine) {
        if (editStockWine == null) {
            log.debug("Edit was not successful. Params: " + editStockWine);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        log.info("Editing stock wine. Params: " + editStockWine);
        stockWineFacade.editStockWine(editStockWine);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<StockWineDto>> getAllStockWines() {
        // retrieve all stock wines from DB
        return new ResponseEntity<>(stockWineFacade.getAllStockWines(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<StockWineDto> getStockInfo(String id) {
        if (id == null) {
            log.info("Provided wine identifier is null.");
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(stockWineFacade.getStockWineById(Long.parseLong(id)), HttpStatus.OK);
    }
}
