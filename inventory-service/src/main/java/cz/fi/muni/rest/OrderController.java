package cz.fi.muni.rest;

import cz.fi.muni.facade.IOrderFacade;
import cz.fi.muni.inventory.api.OrderApiDelegate;
import cz.fi.muni.inventory.model.OrderDto;
import cz.fi.muni.inventory.model.BaseOrderDto;
import cz.fi.muni.inventory.model.OrderResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class OrderController implements OrderApiDelegate {
    private static final Logger log = LoggerFactory.getLogger(StockWineController.class);
    private final IOrderFacade orderFacade;
    @Autowired
    public OrderController(IOrderFacade orderFacade) {
        this.orderFacade = orderFacade;
    }

    /**
     * POST /order : Buy a wine
     *
     * @param orderItem (optional)
     * @return Order accepted (status code 201)
     * or Unexpected error when creating an order. (status code 200)
     * @see OrderApi#buyWines
     */
    @Override
    public ResponseEntity<OrderResponse> buyWines(BaseOrderDto order) {
        if (order == null) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        orderFacade.createOrder(order);

        return new ResponseEntity<>(new OrderResponse() {
            {
                message("Order was accepted.");
            }
        }, HttpStatus.OK);
    }

    /**
     * GET /orders : Get all orders
     * Get all wine orders
     *
     * @param userId (optional)
     * @return OK (status code 200)
     * @see OrderApi#getAllOrders
     */
    @Override
    public ResponseEntity<List<OrderDto>> getAllOrders(String userId) {
        List<OrderDto> orders = orderFacade.getAllOrders(userId);

        return new ResponseEntity<>(orders, HttpStatus.OK);
    }


    /**
     * GET /order/{id} : Get order
     * Get order info by id
     *
     * @param id (required)
     * @return OK (status code 200)
     * @see OrderApi#getOrder
     */
    @Override
    public ResponseEntity<OrderDto> getOrder(String id) {
        if (id == null) {
            log.info("Provided order identifier is null.");
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        OrderDto order = orderFacade.getOrder(Long.parseLong(id));
        return new ResponseEntity<>(order, HttpStatus.OK);
    }

}
