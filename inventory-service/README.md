# Inventory service
The inventory service provides functionality to manage business inventory.
In this case the inventory is taking care of wine bottles.
The following section describe high-level functionality and necessary information to run the service.
More info can be found in openapi specification.

<hr />

## Responsibilities
**From customer perspective:**
1. List all wines
2. Show details about wine
3. Create order
   1. Will trigger change in inventory
   2. Created only if there is enough bottles in inventory
4. Add wine to existing order


**From manager perspective:**
1. Edit wine details
2. Add new wine
3. Show orders
   1. Can be queried by user ID
4. Add bottles to the inventory
5. Remove wine
   1. Will be removed only if there is no bottles in inventory

## How to run
1. `mvn clean install`
2. run `Main`

## How to create new order
To create a new order, one needs to input valid IDs of the desired wines, and the desired quantity of each wine.

