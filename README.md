# [PA165] Enterprise Applications in Java
Project for the PA165 subject at FI MUNI in the spring semester 2023.

**Topic:** Winery Management System

## Team β
- Matej Meško
- Jana Treláková
- Vugar Mammadov


## Assignment 

Create an information system that will allow a person to manage its own winery in a better way.  The main user role can manage the yearly wine harvest, annotating the quality, quantity of different types of grapes. Then he will manage the wines that can be produced depending on the type of grapes available and other ingredients. The user can also manage the list of wine bottles sold and stocked and available to buyers. Buyers can consult the system to buy quantities of wine bottles, also leaving feedback about the quality of the wine they have bought.

## Unforeseen Circumstances
One of our teammates has left the course, due to this one of our services is not fully functional.

<hr />

## Index

* [How to contribute](#how-to-contribute)
* [How to run](#how-to-run)
* [How to run with docker](#how-to-run-with-docker)
* [How to access the services](#how-to-access-the-services)
  - [Security service](#security-service)
  - [Harvesting service](#harvesting-service)
  - [Production service](#production-service)
  - [Inventory service](#inventory-service)
  - [Troubleshooting](#troubleshooting)
* [Database management](#database-management)
* [Observability](#observability)
* [Locust scenario](#locust-scenario)
* [Diagrams](#diagrams)


<hr />

## How to contribute
1. Pull this repository from gitlab
    -  `git clone git@gitlab.fi.muni.cz:xmesko/pa165-winery-management-system-2023.git`
2. Create a new branch
    -  `git checkout -b new-banch-name`
3. Make changes and push them to your branch
4. Create a merge request to `main` branch
    - tag another team member for review 
5. Merge changes to  `main`  branch after approval

<hr />


## How to run

Use this command to run individual services:

```bash
cd ./service-name
mvn clean install
mvn spring-boot:run
```

## How to run with docker

```bash
mvn clean install
docker compose build
docker compose up
```

<hr />

## How to access the services

### Security service

http://localhost:8080/

To access other services, users need to possess a security token.

In order to get this token:

1. Open http://localhost:8080
2. Choose Google or MUNI authentication
3. Log in and choose the Security Scope (for possible future expansion we kept all of them from the seminar, but test_read and test_write are the only ones actually used)
4. Click the button **Get Access Token**
5. Copy this token, or write it down on a piece of paper
6. In each service click **Authorize** and insert your Security Token

For added security we require users to access the services in a browser via http://localhost:PORT/swagger-ui.html

<hr />


### Harvesting service

http://localhost:8081/swagger-ui.html

### Production service
`Due to the fact that one of our colleagues has left the course, this service is not fully functional.`

http://localhost:8083/swagger-ui.html

### Inventory service

http://localhost:8082/swagger-ui.html

<hr />

#### Troubleshooting
In certain cases some of the services might not start properly.
This is because of a deficiency with docker `depends_on`, and the fact that databases take longer to start and initialise the first time.
Due to this, the services might start before the database is ready.

In this case, just **restart** the service.

<hr />


## Database management
### Cleanup
To clean all three databases run this command from root directory of the project.
```bash
./database-management/clean-all.sh
```
Run this command to clean inventory-service-db. For more examples see [clean-all](database-management/clean-all.sh) script.

```bash
docker exec -i inventory-service-db bash < ./database-management/clean.sh
```

### Seeding
To seed all three databases run this command from root directory of the project.
```bash
./database-management/seed-all.sh
```

Run this command to seed inventory-service-db. For more examples see [seed-all](database-management/seed-all.sh) script.
```bash
./database-management/seed.sh inventory-service-db database-management/INVENTORY-SEED.sql
```
<hr />


# Observability
To see Grafana dashboard go to http://localhost:3000.

Go to Menu -> Dashboard -> General (New Dashboard) 

or go to http://localhost:3000/dashboards. 

Grafana credentials: `admin` + `admin`.

<hr />

## Locust scenario

To run locust scenario, you need to have locust and all dependencies installed.

1. Run the services
2. Get security token from security service
3. Modify the locustfile.py and insert your token
4. cd to locust directory (./locust)
5. Run locust
6. Open http://localhost:8089
7. Insert number of users and spawn rate
8. Click start swarming
9. Enjoy


<hr />

## Diagrams
### Use case diagram

![Use case diagram](diagrams/useCaseDiagram.svg)

### Class diagrams

![Class diagram](diagrams/classDiagram.svg)