package cz.fi.muni.facade.ingredient;

import cz.fi.muni.api.requestDto.IngredientRequestDto;
import cz.fi.muni.api.responseDto.IngredientResponseDto;
import cz.fi.muni.data.model.Ingredient;
import cz.fi.muni.mapper.IngredientMapper;
import cz.fi.muni.service.ingredient.IngredientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IngredientFacade implements IIngredientFacade{

    private final IngredientMapper ingredientMapper;
    private final IngredientService ingredientService;

    @Autowired
    public IngredientFacade(IngredientMapper ingredientMapper, IngredientService ingredientService) {
        this.ingredientMapper = ingredientMapper;
        this.ingredientService = ingredientService;
    }

    @Override
    public void create(IngredientRequestDto requestDto) {
        Ingredient ingredient = ingredientMapper.toEntity(requestDto);
        ingredientService.create(ingredient);
    }

    @Override
    public IngredientResponseDto getById(Integer id) {
        return ingredientMapper.toResponseDto(ingredientService.getById(id));
    }

    @Override
    public List<IngredientResponseDto> getAll() {
        return ingredientMapper.toResponseDtoList(ingredientService.getAll());
    }

    @Override
    public void update(Integer id, IngredientRequestDto requestDto) {
        Ingredient ingredient = ingredientMapper.toEntity(requestDto);
        ingredientService.update(id, ingredient);
    }

    @Override
    public void delete(Integer id) {
        ingredientService.delete(id);
    }

}
