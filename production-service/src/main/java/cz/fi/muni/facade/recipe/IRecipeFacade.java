package cz.fi.muni.facade.recipe;

import cz.fi.muni.api.requestDto.IngredientRequestDto;
import cz.fi.muni.api.requestDto.RecipeRequestDto;
import cz.fi.muni.api.responseDto.IngredientResponseDto;
import cz.fi.muni.api.responseDto.RecipeResponseDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

public interface IRecipeFacade {

    void create(RecipeRequestDto requestDto);

    RecipeResponseDto getById(Integer id);

    List<RecipeResponseDto> getAll();

    void update(Integer id, RecipeRequestDto requestDto);

}
