package cz.fi.muni.facade.ingredient;

import cz.fi.muni.api.requestDto.IngredientRequestDto;
import cz.fi.muni.api.responseDto.IngredientResponseDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

public interface IIngredientFacade {

    void create(IngredientRequestDto requestDto);

    IngredientResponseDto getById(Integer id);

    List<IngredientResponseDto> getAll();

    void update(Integer id, IngredientRequestDto requestDto);

    void delete(Integer id);

}
