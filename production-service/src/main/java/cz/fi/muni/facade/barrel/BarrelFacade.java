package cz.fi.muni.facade.barrel;

import cz.fi.muni.api.requestDto.BarrelRequestDto;
import cz.fi.muni.api.responseDto.BarrelResponseDto;
import cz.fi.muni.data.model.Barrel;
import cz.fi.muni.mapper.BarrelMapper;
import cz.fi.muni.service.barrel.BarrelService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class BarrelFacade implements IBarrelFacade{

    private final BarrelMapper barrelMapper;
    private final BarrelService barrelService;

    @Override
    public void create(BarrelRequestDto requestDto) {
        Barrel barrel = barrelMapper.toEntity(requestDto);
        barrelService.create(barrel);
    }

    @Override
    public BarrelResponseDto getById(Integer id) {
        return barrelMapper.toResponseDto(barrelService.getById(id));
    }

    @Override
    public List<BarrelResponseDto> getAll() {
        return barrelMapper.toResponseDtoList(barrelService.getAll());
    }

    @Override
    public void update(Integer id, BarrelRequestDto requestDto) {
        Barrel barrel = barrelMapper.toEntity(requestDto);
        barrelService.update(id, barrel);
    }

    @Override
    public void delete(Integer id) {
        barrelService.delete(id);
    }

    @Override
    public byte[] export(Integer id) {
        return barrelService.export(id);
    }

}
