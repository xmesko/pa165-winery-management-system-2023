package cz.fi.muni.facade.barrel;

import cz.fi.muni.api.requestDto.BarrelRequestDto;
import cz.fi.muni.api.requestDto.IngredientRequestDto;
import cz.fi.muni.api.responseDto.BarrelResponseDto;
import cz.fi.muni.api.responseDto.IngredientResponseDto;

import java.util.List;

public interface IBarrelFacade {

    void create(BarrelRequestDto requestDto);

    BarrelResponseDto getById(Integer id);

    List<BarrelResponseDto> getAll();

    void update(Integer id, BarrelRequestDto requestDto);

    void delete(Integer id);

    byte[] export(Integer id);

}
