package cz.fi.muni.facade.recipe;

import cz.fi.muni.api.requestDto.RecipeIngredientRequestDto;
import cz.fi.muni.api.requestDto.RecipeRequestDto;
import cz.fi.muni.api.responseDto.IngredientResponseDto;
import cz.fi.muni.api.responseDto.RecipeResponseDto;
import cz.fi.muni.data.model.Ingredient;
import cz.fi.muni.data.model.Recipe;
import cz.fi.muni.data.repository.IngredientRepository;
import cz.fi.muni.mapper.IngredientMapper;
import cz.fi.muni.mapper.RecipeMapper;
import cz.fi.muni.service.recipe.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RecipeFacade implements IRecipeFacade {

    private final RecipeMapper recipeMapper;
    private final IngredientRepository ingredientRepository;
    private final IngredientMapper ingredientMapper;
    private final RecipeService recipeService;

    @Autowired
    public RecipeFacade(RecipeMapper recipeMapper, IngredientRepository ingredientRepository, IngredientMapper ingredientMapper, RecipeService recipeService) {
        this.recipeMapper = recipeMapper;
        this.ingredientRepository = ingredientRepository;
        this.ingredientMapper = ingredientMapper;
        this.recipeService = recipeService;
    }

    @Override
    public void create(RecipeRequestDto requestDto) {
        Recipe recipe = recipeMapper.toEntity(requestDto);
        addIngredients(recipe, requestDto);
        recipeService.create(recipe);
    }

    @Override
    public RecipeResponseDto getById(Integer id) {
        Recipe recipe = recipeService.getById(id);
        RecipeResponseDto recipeResponseDto =  recipeMapper.toResponseDto(recipe);
        List<IngredientResponseDto> ingredientResponseDtos = new ArrayList<>();
        for (Ingredient ingredient: recipe.getOtherIngredients()){
            ingredientResponseDtos.add(ingredientMapper.toResponseDto(ingredient));
        }
        recipeResponseDto.setOtherIngredients(ingredientResponseDtos);
        return recipeResponseDto;
    }

    @Override
    public List<RecipeResponseDto> getAll() {
        List<Recipe> recipes = recipeService.getAll();
        List<RecipeResponseDto> recipeResponseDtoList =  new ArrayList<>();
        List<IngredientResponseDto> ingredientResponseDtos = new ArrayList<>();
        for (Recipe recipe: recipes){
            for (Ingredient ingredient: recipe.getOtherIngredients()){
                ingredientResponseDtos.add(ingredientMapper.toResponseDto(ingredient));
            }
            RecipeResponseDto recipeResponseDto = recipeMapper.toResponseDto(recipe);
            recipeResponseDto.setOtherIngredients(ingredientResponseDtos);
            recipeResponseDtoList.add(recipeResponseDto);
        }
        return recipeResponseDtoList;
    }

    @Override
    public void update(Integer id, RecipeRequestDto requestDto) {
        Recipe recipe = recipeMapper.toEntity(requestDto);
//        addIngredients(recipe, requestDto);
        recipeService.update(id, recipe);
    }

    private void addIngredients(Recipe recipe, RecipeRequestDto requestDto) {
        List<Ingredient> ingredients = new ArrayList<>();
        for (RecipeIngredientRequestDto ingredientRequestDto : requestDto.getOtherIngredients()) {
            Ingredient ingredient = ingredientRepository.getByName(ingredientRequestDto.getName());
            ingredients.add(ingredient);
        }
        recipe.setOtherIngredients(ingredients);
    }

}
