package cz.fi.muni.data.repository;

import cz.fi.muni.data.model.Barrel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface BarrelRepository extends JpaRepository<Barrel, Integer> {



}
