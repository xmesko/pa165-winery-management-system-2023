package cz.fi.muni.data.model;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Barrel")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Barrel implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    Integer volume;
    @Column(name = "alcohol_level")
    Double alcoholLevel;
    @Column(name = "sugar_level")
    Double sugarLevel;
    Integer acidity;
    @Column(name = "fill_date")
    LocalDate fillDate;

}
