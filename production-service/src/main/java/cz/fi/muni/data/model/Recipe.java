package cz.fi.muni.data.model;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Recipe")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Recipe implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    @Column(name = "technical_process")
    String technicalProcess;
    @Column(name = "grape-wine_ratio")
    Double grapeWineRatio;
    @Column(name = "aging_time")
    Integer agingTime;
    @OneToMany
    List<Ingredient> otherIngredients;

}
