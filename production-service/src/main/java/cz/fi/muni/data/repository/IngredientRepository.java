package cz.fi.muni.data.repository;

import cz.fi.muni.data.model.Ingredient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
@ComponentScan
public interface IngredientRepository extends JpaRepository<Ingredient, Integer> {

    Ingredient getByName(String name);

}
