package cz.fi.muni.service.mail;

import cz.fi.muni.data.model.Notification;
import cz.fi.muni.data.repository.NotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class NotificationService {
    private final NotificationRepository notificationRepository;
    private final EmailSender emailNotificationService;

    @Autowired
    public NotificationService(NotificationRepository notificationRepository, EmailSender emailNotificationService) {
        this.notificationRepository = notificationRepository;
        this.emailNotificationService = emailNotificationService;
    }

    public List<Notification> getNotificationsToBeSent() {
        LocalDate now = LocalDate.now();
        return notificationRepository.findBySendDateBefore(now);
    }

    public void sendNotification(Notification notification) throws Exception {
        emailNotificationService.sendEmail(notification.getTo(), notification.getSubject(), notification.getText());
        notificationRepository.delete(notification);
    }

}
