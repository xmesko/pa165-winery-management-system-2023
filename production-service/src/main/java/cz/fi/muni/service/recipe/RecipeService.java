package cz.fi.muni.service.recipe;

import cz.fi.muni.data.model.Ingredient;
import cz.fi.muni.data.model.Notification;
import cz.fi.muni.data.model.Recipe;
import cz.fi.muni.data.repository.IngredientRepository;
import cz.fi.muni.data.repository.NotificationRepository;
import cz.fi.muni.data.repository.RecipeRepository;
import cz.fi.muni.exceptions.EntityNotFoundException;
import cz.fi.muni.mapper.IngredientMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;

@Service
public class RecipeService implements IRecipeService {

    private final IngredientRepository ingredientRepository;
    private final RecipeRepository recipeRepository;
    private final NotificationRepository notificationRepository;

    @Autowired
    public RecipeService(IngredientRepository ingredientRepository, RecipeRepository recipeRepository, IngredientMapper ingredientMapper, NotificationRepository notificationRepository) {
        this.ingredientRepository = ingredientRepository;
        this.recipeRepository = recipeRepository;
        this.notificationRepository = notificationRepository;
    }

    @Override
    public Recipe create(Recipe recipe) {
        Notification notification = new Notification();
        notification.setTo("winerycompany2022@gmail.com"); // temporary mail , we can change it
        notification.setSubject("Wine aging time");
        notification.setText("We are writing to inform you that the aging time for your wine has been completed. ");
        notification.setSendDate(LocalDate.now().plusDays(recipe.getAgingTime()));
        notificationRepository.save(notification);
        return recipeRepository.save(recipe);
    }

    @Override
    public Recipe getById(Integer id) {
        return recipeRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(Recipe.class, id));
    }

    @Override
    public List<Recipe> getAll() {
        return recipeRepository.findAll();
    }

    @Override
    public Recipe update(Integer id, Recipe recipe) {
        Recipe existingRecipe = recipeRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(Recipe.class, id));
        existingRecipe.setAgingTime(recipe.getAgingTime());
        existingRecipe.setGrapeWineRatio(recipe.getGrapeWineRatio());
        existingRecipe.setTechnicalProcess(recipe.getTechnicalProcess());
        existingRecipe.setOtherIngredients(recipe.getOtherIngredients());
        Recipe newRecipe = recipeRepository.save(existingRecipe);
        updateRecipeIngredients(recipe.getOtherIngredients());
        return newRecipe;

    }

    private void updateRecipeIngredients(List<Ingredient> ingredients) {
        if (Objects.isNull(ingredients)) {
            return;
        }

        for (Ingredient newIngredient : ingredients) {
            Ingredient existingIngredient = ingredientRepository.getByName(newIngredient.getName());
            if (existingIngredient.getAmount() > newIngredient.getAmount())
                existingIngredient.setAmount(existingIngredient.getAmount() - newIngredient.getAmount());
            ingredientRepository.save(existingIngredient); // Save the existingIngredient
        }
        ingredientRepository.flush(); // Flush the changes to the database
    }

    public void mailSender(){

    }

}
