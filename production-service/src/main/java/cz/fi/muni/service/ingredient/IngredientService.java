package cz.fi.muni.service.ingredient;

import cz.fi.muni.data.model.Ingredient;
import cz.fi.muni.data.repository.IngredientRepository;
import cz.fi.muni.exceptions.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IngredientService implements IIngredientService {

    private final IngredientRepository ingredientRepository;

    @Autowired
    public IngredientService(IngredientRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }

    @Override
    public Ingredient create(Ingredient ingredient) {
        return ingredientRepository.save(ingredient);
    }

    @Override
    public Ingredient getById(Integer id) {
        return ingredientRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(Ingredient.class, id));
    }

    @Override
    public List<Ingredient> getAll() {
        return (List<Ingredient>) ingredientRepository.findAll();
    }

    @Override
    public Ingredient update(Integer id, Ingredient ingredient) {
        Ingredient existingIngredient = ingredientRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(Ingredient.class, id));
        existingIngredient.setName(ingredient.getName());
        existingIngredient.setAmount(ingredient.getAmount());
        existingIngredient.setPrice(ingredient.getPrice());
        return ingredientRepository.save(existingIngredient);
    }

    @Override
    public void delete(Integer id) {
        ingredientRepository.deleteById(id);
    }

}
