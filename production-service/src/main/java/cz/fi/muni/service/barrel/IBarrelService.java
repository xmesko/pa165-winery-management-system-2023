package cz.fi.muni.service.barrel;

import cz.fi.muni.api.requestDto.BarrelRequestDto;
import cz.fi.muni.data.model.Barrel;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

public interface IBarrelService {


    Barrel create(Barrel barrel);

    Barrel getById(Integer id);

    List<Barrel> getAll();

    Barrel update(Integer id, Barrel barrel);

    void delete(Integer id);

    byte[] export(Integer id) throws IOException;

}
