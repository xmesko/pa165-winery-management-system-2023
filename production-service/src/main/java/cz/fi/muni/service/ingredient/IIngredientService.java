package cz.fi.muni.service.ingredient;

import cz.fi.muni.data.model.Ingredient;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

public interface IIngredientService {

    Ingredient create(Ingredient ingredient);

    Ingredient getById(Integer id);

    List<Ingredient> getAll();

    Ingredient update(Integer id, Ingredient ingredient);

    void delete(Integer id);

}
