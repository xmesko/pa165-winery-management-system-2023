package cz.fi.muni.service.recipe;

import cz.fi.muni.data.model.Ingredient;
import cz.fi.muni.data.model.Recipe;

import java.util.List;
import java.util.UUID;

public interface IRecipeService {

    Recipe create(Recipe recipe);

    Recipe getById(Integer id);

    List<Recipe> getAll();

    Recipe update(Integer id, Recipe recipe);

}
