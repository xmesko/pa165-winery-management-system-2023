package cz.fi.muni.service.mail;

import cz.fi.muni.data.model.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class NotificationTask {
    @Autowired
    private NotificationService notificationService;

    @Scheduled(cron = "0 0 * * * *")
    public void checkNotifications() throws Exception {
        // Retrieve any notifications that need to be sent
        List<Notification> notifications = notificationService.getNotificationsToBeSent();

        // Send each notification
        for (Notification notification : notifications) {
            notificationService.sendNotification(notification);
        }
    }

}
