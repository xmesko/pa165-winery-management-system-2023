package cz.fi.muni.service.barrel;

import cz.fi.muni.data.model.Barrel;
import cz.fi.muni.data.repository.BarrelRepository;
import cz.fi.muni.exceptions.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class BarrelService implements IBarrelService {

    private final BarrelRepository barrelRepository;

    @Override
    public Barrel create(Barrel barrel) {
        return barrelRepository.save(barrel);
    }

    @Override
    public Barrel getById(Integer id) {
        return barrelRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(Barrel.class, id));
    }

    @Override
    public List<Barrel> getAll() {
        return (List<Barrel>) barrelRepository.findAll();
    }

    @Override
    public Barrel update(Integer id, Barrel barrel) {
        Barrel existingBarrel = barrelRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(Barrel.class, id));
        existingBarrel.setVolume(barrel.getVolume());
        existingBarrel.setAlcoholLevel(barrel.getAlcoholLevel());
        existingBarrel.setSugarLevel(barrel.getSugarLevel());
        existingBarrel.setAcidity(barrel.getAcidity());
        existingBarrel.setFillDate(barrel.getFillDate());
        return barrelRepository.save(existingBarrel);
    }

    @Override
    public void delete(Integer id) {
        barrelRepository.deleteById(id);
    }

    @Override
    @SneakyThrows
    public byte[] export(Integer id){
        Barrel barrel = barrelRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(Barrel.class, id));
        XWPFDocument document = new XWPFDocument();

        XWPFParagraph documentTitle = document.createParagraph();
        documentTitle.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun run = documentTitle.createRun();
        run.setText("Wine Barrel Report");
        run.setBold(true);
        run.setFontSize(14);
        run.addBreak();
        run.addBreak();

        // It is just for template, we can change context if you want
        XWPFParagraph bodyMain = document.createParagraph();
        bodyMain.setAlignment(ParagraphAlignment.LEFT);
        run = bodyMain.createRun();
        run.setText("Barrel ID: " + barrel.getId());
        run.addBreak();
        run.setText("Wine Acidity: " + barrel.getAcidity());
        run.addBreak();
        run.setText("Barrel volume: " + barrel.getVolume());
        run.addBreak();
        run.setText("Wine alcohol level: " + barrel.getAlcoholLevel());
        run.addBreak();
        run.setText("Wine sugar level: " + barrel.getSugarLevel());
        run.addBreak();
        run.setText("Barrel fill date: " + barrel.getFillDate());
        run.addBreak();

        run.setFontSize(12);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        document.write(baos);
        baos.close();
        document.close();
        return baos.toByteArray();
    }

}
