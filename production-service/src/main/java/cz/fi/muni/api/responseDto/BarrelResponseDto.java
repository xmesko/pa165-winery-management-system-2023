package cz.fi.muni.api.responseDto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import java.time.LocalDate;
import java.util.UUID;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BarrelResponseDto {

    Integer id;
    Integer volume;
    Double alcoholLevel;
    Double sugarLevel;
    Integer acidity;
    LocalDate fillDate;

}
