package cz.fi.muni.api.requestDto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.List;
import java.util.UUID;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RecipeRequestDto {

    String technicalProcess;
    Double grapeWineRatio;
    Integer agingTime;
    List<RecipeIngredientRequestDto> otherIngredients;
    List<String> deleteIngredients;

}
