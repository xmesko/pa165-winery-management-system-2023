package cz.fi.muni.api.requestDto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BarrelRequestDto {

    Integer volume;
    Double alcoholLevel;
    Double sugarLevel;
    Integer acidity;
    LocalDate fillDate;

}
