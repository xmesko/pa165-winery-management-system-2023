package cz.fi.muni.api.requestDto;

import jakarta.validation.constraints.NotBlank;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class IngredientRequestDto {

    @NotBlank
    String name;
    Integer amount;
    Double price;
}
