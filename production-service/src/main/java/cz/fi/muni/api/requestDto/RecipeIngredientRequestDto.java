package cz.fi.muni.api.requestDto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RecipeIngredientRequestDto {

    String name;
    Integer amount;

}
