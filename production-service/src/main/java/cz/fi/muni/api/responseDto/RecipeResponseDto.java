package cz.fi.muni.api.responseDto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.List;
import java.util.UUID;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RecipeResponseDto {

    Integer id;
    String technicalProcess;
    Double grapeWineRatio;
    Integer agingTime;
    List<IngredientResponseDto> otherIngredients;

}
