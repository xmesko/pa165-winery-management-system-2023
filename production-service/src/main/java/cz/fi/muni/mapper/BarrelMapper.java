package cz.fi.muni.mapper;

import cz.fi.muni.api.requestDto.BarrelRequestDto;
import cz.fi.muni.api.responseDto.BarrelResponseDto;
import cz.fi.muni.data.model.Barrel;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        componentModel = "spring")
public interface BarrelMapper {

    Barrel toEntity(BarrelRequestDto requestDto);

    BarrelResponseDto toResponseDto(Barrel barrel);

    List<BarrelResponseDto> toResponseDtoList(List<Barrel> barrels);

}
