package cz.fi.muni.mapper;

import cz.fi.muni.api.requestDto.RecipeRequestDto;
import cz.fi.muni.api.responseDto.RecipeResponseDto;
import cz.fi.muni.data.model.Ingredient;
import cz.fi.muni.data.model.Recipe;
import org.mapstruct.*;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        componentModel = "spring", uses = {IngredientMapper.class})
public interface RecipeMapper {
    @Mapping(target = "otherIngredients", source = "otherIngredients", ignore = true)
    Recipe toEntity(RecipeRequestDto requestDto);

    RecipeResponseDto toResponseDto(Recipe recipe);

    List<RecipeResponseDto> toResponseDtoList(List<Recipe> recipes);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(target = "otherIngredients", source = "otherIngredients", ignore = true)
    void updateEntity(@MappingTarget Recipe recipe, RecipeRequestDto requestDto);

}
