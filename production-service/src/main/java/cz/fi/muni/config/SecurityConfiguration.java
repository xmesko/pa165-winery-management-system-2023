package cz.fi.muni.config;


import io.swagger.v3.oas.models.security.*;
import lombok.var;
import org.springdoc.core.customizers.OpenApiCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.resource.OAuth2ResourceServerConfigurer;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebSecurity
@EnableWebMvc
public class SecurityConfiguration {


    @Bean
    public OpenApiCustomizer openAPICustomizer() {
        return openApi -> {
            openApi.getComponents()
                    .addSecuritySchemes("Bearer",
                            new SecurityScheme()
                                    .type(SecurityScheme.Type.HTTP)
                                    .scheme("bearer")
                                    .description("provide a valid access token")
                    );
            var managerScope = new SecurityRequirement().addList("Bearer", "test_write");

            openApi.getPaths().get("/api/barrels").getPost().addSecurityItem(managerScope);
            openApi.getPaths().get("/api/barrels/{id}").getDelete().addSecurityItem(managerScope);
            openApi.getPaths().get("/api/barrels").getGet().addSecurityItem(managerScope);
            openApi.getPaths().get("/api/barrels/{id}").getGet().addSecurityItem(managerScope);
            openApi.getPaths().get("/api/barrels/{id}").getPut().addSecurityItem(managerScope);
            openApi.getPaths().get("/api/barrels/{id}/export").getGet().addSecurityItem(managerScope);

            openApi.getPaths().get("/api/ingredients").getPost().addSecurityItem(managerScope);
            openApi.getPaths().get("/api/ingredients/{id}").getDelete().addSecurityItem(managerScope);
            openApi.getPaths().get("/api/ingredients").getGet().addSecurityItem(managerScope);
            openApi.getPaths().get("/api/ingredients/{id}").getGet().addSecurityItem(managerScope);
            openApi.getPaths().get("/api/ingredients/{id}").getPut().addSecurityItem(managerScope);
            
            openApi.getPaths().get("/api/recipes").getPost().addSecurityItem(managerScope);
            openApi.getPaths().get("/api/recipes").getGet().addSecurityItem(managerScope);
            openApi.getPaths().get("/api/recipes/{id}").getGet().addSecurityItem(managerScope);
            openApi.getPaths().get("/api/recipes/{id}").getPut().addSecurityItem(managerScope);
            
        };
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.authorizeHttpRequests(x -> x
                        .requestMatchers("/swagger-ui/**", "/v3/api-docs/**", "/").permitAll()

                        .requestMatchers("/api/barrels").hasAuthority("SCOPE_test_write")
                        .requestMatchers("/api/barrels/**").hasAuthority("SCOPE_test_write")
                        .requestMatchers("/api/ingredients").hasAuthority("SCOPE_test_write")
                        .requestMatchers("/api/ingredients/**").hasAuthority("SCOPE_test_write")
                        .requestMatchers("/api/recipes").hasAuthority("SCOPE_test_write")
                        .requestMatchers("/api/recipes/**").hasAuthority("SCOPE_test_write")

                        .anyRequest().permitAll()
                )
                .oauth2ResourceServer(OAuth2ResourceServerConfigurer::opaqueToken)
        ;
        return http.build();
    }

}
