package cz.fi.muni.controller;

import cz.fi.muni.api.requestDto.IngredientRequestDto;
import cz.fi.muni.api.responseDto.BarrelResponseDto;
import cz.fi.muni.api.responseDto.IngredientResponseDto;
import cz.fi.muni.facade.ingredient.IIngredientFacade;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController()
@RequestMapping("/api/ingredients")
public class IngredientController {
    private static final Logger log = LoggerFactory.getLogger(IngredientController.class);
    private final IIngredientFacade ingredientFacade;

    @Autowired
    public IngredientController(IIngredientFacade ingredientFacade) {
        this.ingredientFacade = ingredientFacade;
    }

    @PostMapping
    public ResponseEntity<Void> addIngredient(@RequestBody IngredientRequestDto requestDto) {
        if (requestDto == null) {
            log.info("No ingredient to be created provided.");
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        ingredientFacade.create(requestDto);
        return new ResponseEntity<>(null, HttpStatus.CREATED);

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteIngredient(@PathVariable Integer id) {
        if (id == null) {
            log.info("Provided ingredient identifier is null.");
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        ingredientFacade.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<IngredientResponseDto>> getAllIngredients() {
        log.info("getAllIngredients");
        return new ResponseEntity<>(ingredientFacade.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<IngredientResponseDto> getIngredient(@PathVariable Integer id) {
        if (id == null) {
            log.info("Provided ingredient identifier is null.");
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(ingredientFacade.getById(id), HttpStatus.OK);

    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> editIngredient(@PathVariable Integer id, @RequestBody IngredientRequestDto requestDto) {
        if (id == null || requestDto == null) {
            log.debug("Edit was not successful. ID: " + id + " Params: " + requestDto);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        ingredientFacade.update(id, requestDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
