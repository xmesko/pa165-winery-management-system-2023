package cz.fi.muni.controller;

import cz.fi.muni.api.requestDto.BarrelRequestDto;
import cz.fi.muni.api.responseDto.BarrelResponseDto;
import cz.fi.muni.facade.barrel.IBarrelFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/barrels")
public class BarrelController {
    private static final Logger log = LoggerFactory.getLogger(BarrelController.class);
    private final IBarrelFacade barrelFacade;

    @Autowired
    public BarrelController(IBarrelFacade barrelFacade) {
        this.barrelFacade = barrelFacade;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void addBarrel(@RequestBody BarrelRequestDto requestDto) {
        if (requestDto == null) {
            log.info("No barrel to be created provided.");
        }
        barrelFacade.create(requestDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteBarrel(@PathVariable Integer id) {
        if (id == null) {
            log.info("Provided barrel identifier is null.");
        }
        barrelFacade.delete(id);
    }


    @GetMapping
    public ResponseEntity<List<BarrelResponseDto>> getAllBarrels() {
        log.info("getAllBarrels");
        return new ResponseEntity<>(barrelFacade.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BarrelResponseDto> getBarrel(@PathVariable Integer id) {
        System.out.println(id);
        if (id == null) {
            log.info("Provided barrel identifier is null.");
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(barrelFacade.getById(id), HttpStatus.OK);
    }


    @PutMapping("/{id}")
    public ResponseEntity<Void> editBarrel(@PathVariable Integer id, @RequestBody BarrelRequestDto requestDto) {
        if (id == null || requestDto == null) {
            log.debug("Edit was not successful. ID: " + id + " Params: " + requestDto);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        barrelFacade.update(id, requestDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{id}/export")
    public ResponseEntity<byte[]> export(@PathVariable Integer id) {
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=barrel.docx")
                .header(HttpHeaders.CONTENT_TYPE, "application/msword")
                .body(barrelFacade.export(id));
    }

}
