package cz.fi.muni.controller;

import cz.fi.muni.api.requestDto.RecipeRequestDto;
import cz.fi.muni.api.responseDto.RecipeResponseDto;
import cz.fi.muni.facade.recipe.IRecipeFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/recipes")
public class RecipeController {
    private static final Logger log = LoggerFactory.getLogger(RecipeController.class);
    private final IRecipeFacade recipeFacade;

    @Autowired
    public RecipeController(IRecipeFacade recipeFacade) {
        this.recipeFacade = recipeFacade;
    }

    @PostMapping
    public ResponseEntity<Void> addRecipe(@RequestBody RecipeRequestDto requestDto) {
        if (requestDto == null) {
            log.info("No recipe to be created provided.");
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        recipeFacade.create(requestDto);
        return new ResponseEntity<>(null, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<RecipeResponseDto>> getAllRecipes() {
        log.info("getAllRecipes");
        return new ResponseEntity<>(recipeFacade.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<RecipeResponseDto> getRecipe(@PathVariable Integer id) {
        if (id == null) {
            log.info("Provided recipe identifier is null.");
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(recipeFacade.getById(id), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> editRecipe(@PathVariable Integer id, @RequestBody RecipeRequestDto requestDto) {
        if (id == null || requestDto == null) {
            log.debug("Edit was not successful. ID: " + id + " Params: " + requestDto);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        recipeFacade.update(id, requestDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }


}
