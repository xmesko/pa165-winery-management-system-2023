package cz.fi.muni.controller;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;

@AutoConfigureMockMvc(addFilters = false)


public class IngredientControllerTest {
    @Test
    void addIngredient() {
    }

    @Test
    void deleteIngredient() {
    }

    @Test
    void getAllIngredients() {
    }

    @Test
    void getIngredient() {
    }

    @Test
    void editIngredient() {
    }
}
