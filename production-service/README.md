# Production service
The production service provides functionality to manage wine production.
in this service, which ingredients are used during production, according to which recipe wine is prepared, 
and store information is managed. The following section describes high-level functionality and necessary information 
to run the service. Further info can be found in the openapi specification.

# Incomplete functionality
Some of the parts in this service are not fully implemented, as our colleague who was responsible for this part of the project
was unfortunately not able to finish it before he left the project.

<hr />

## Responsibilities
1. Add new ingredient and recipe
2. Edit ingredients, recipe and barrel
3. Delete specific ingredient



## How to run
1. `mvn clean install`
2. run `Main`

