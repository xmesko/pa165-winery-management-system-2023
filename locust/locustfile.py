from locust import HttpUser, task, between


class HarvestingManager(HttpUser):

    wait_time = between(5, 10)
    auth_header = {"Authorization": "Bearer eyJraWQiOiJyc2ExIiwidHlwIjoiYXQrand0IiwiYWxnIjoiUlMyNTYifQ.eyJhdWQiOiI3ZTAyYTBhOS00NDZhLTQxMmQtYWQyYi05MGFkZDQ3YjBmZGQiLCJzdWIiOiI0ODU0NzJAbXVuaS5jeiIsImFjciI6Imh0dHBzOi8vcmVmZWRzLm9yZy9wcm9maWxlL3NmYSIsInNjb3BlIjoidGVzdF80IHRlc3RfMyB0ZXN0XzIgdGVzdF8xIHRlc3RfcmVhZCBvcGVuaWQgcHJvZmlsZSBlZHVwZXJzb25fc2NvcGVkX2FmZmlsaWF0aW9uIHRlc3Rfd3JpdGUgZW1haWwgdGVzdF81IiwiYXV0aF90aW1lIjoxNjgzOTg3MzAyLCJpc3MiOiJodHRwczovL29pZGMubXVuaS5jei9vaWRjLyIsImV4cCI6MTY4NDA2OTA0MCwiaWF0IjoxNjg0MDY1NDQwLCJjbGllbnRfaWQiOiI3ZTAyYTBhOS00NDZhLTQxMmQtYWQyYi05MGFkZDQ3YjBmZGQiLCJqdGkiOiI5NDk4MjEzMy1jMDZlLTRlYjgtYmZkYS04Mzg2MDkwNDNmYjAifQ.xLGpwHXu-mj7AMLsltmqL8QyK9RjvF1C1PoDueAjyMDHQH-nIu8TNYUf2eeR8-yr03jDDTw4kQ-DTVxmaYGjZ4pJCkED2S6QJKFNzX70bC1ztbuoahvUEd3tonO8zPH-apuBtn0ZqEk0zS_yEx_zBw6DHYh3DZkRVjE-3MPmbC-xgghnCFvfzE_oHoGicMMzt-NWBV6KDZ0Lme1s3HVPhGGsEf268m5DOqVNcA8v7biugpTaS_8IwNNaJ8zA0oVIVPzkwTNOScIe58N2Fid38OSbp98gda4HEmrU499idIuKwIqtLwVl9Eoz_-WPBckQJN8XWuPWUTiQQwqIwckOHQ"}
    #auth_header = {"Authorization": "Bearer "}

    host = "http://localhost:8081"

    raw_grape_id = 1
    wine_id = 1

    # add a new field
    @task(1)
    def add_field(self):
        self.client.post("/field", json={
            "variety": "Pálava",
            "harvestSize": 2000,
            "harvestDate": "2023-09-24"
        }, headers=self.auth_header)

    # get info about a field
    @task(2)
    def get_field(self):
        self.client.get("/field/1", headers=self.auth_header)

    # edit a field
    @task(3)
    def edit_field(self):
        self.client.patch("/field/1", json={
            "variety": "Pálava",
            "harvestSize": 2400,
            "harvestDate": "2023-09-24"
        }, headers=self.auth_header)

    # get all fields
    @task(4)
    def get_all_fields(self):
        self.client.get("/fields", headers=self.auth_header)

    # harvest a field
    @task(5)
    def harvest_field(self):
        self.client.post("/field/1", headers=self.auth_header)

    # delete a field
    #@task(6)
    #def delete_field(self):
    #    self.client.delete("/field/1", headers=self.auth_header)

    # get raw_grape info
    @task(7)
    def get_raw_grape(self):
        self.client.get("/rawgrape/{}".format(self.raw_grape_id), headers=self.auth_header)

    # edit raw_grape
    @task(8)
    def edit_raw_grape(self):
        self.client.patch("/rawgrape/{}".format(self.raw_grape_id), json={
            "id": 1,
            "amount": 1800,
            "variety": "Pálava",
            "quality": "World Class"
        }, headers=self.auth_header)

    # get all raw_grapes
    @task(9)
    def get_all_raw_grapes(self):
        self.client.get("/rawgrapes", headers=self.auth_header)


class WineManager(HttpUser):
    wait_time = between(5, 10)
    auth_header = {"Authorization": "Bearer eyJraWQiOiJyc2ExIiwidHlwIjoiYXQrand0IiwiYWxnIjoiUlMyNTYifQ.eyJhdWQiOiI3ZTAyYTBhOS00NDZhLTQxMmQtYWQyYi05MGFkZDQ3YjBmZGQiLCJzdWIiOiI0ODU0NzJAbXVuaS5jeiIsImFjciI6Imh0dHBzOi8vcmVmZWRzLm9yZy9wcm9maWxlL3NmYSIsInNjb3BlIjoidGVzdF80IHRlc3RfMyB0ZXN0XzIgdGVzdF8xIHRlc3RfcmVhZCBvcGVuaWQgcHJvZmlsZSBlZHVwZXJzb25fc2NvcGVkX2FmZmlsaWF0aW9uIHRlc3Rfd3JpdGUgZW1haWwgdGVzdF81IiwiYXV0aF90aW1lIjoxNjgzOTg3MzAyLCJpc3MiOiJodHRwczovL29pZGMubXVuaS5jei9vaWRjLyIsImV4cCI6MTY4NDA2OTA0MCwiaWF0IjoxNjg0MDY1NDQwLCJjbGllbnRfaWQiOiI3ZTAyYTBhOS00NDZhLTQxMmQtYWQyYi05MGFkZDQ3YjBmZGQiLCJqdGkiOiI5NDk4MjEzMy1jMDZlLTRlYjgtYmZkYS04Mzg2MDkwNDNmYjAifQ.xLGpwHXu-mj7AMLsltmqL8QyK9RjvF1C1PoDueAjyMDHQH-nIu8TNYUf2eeR8-yr03jDDTw4kQ-DTVxmaYGjZ4pJCkED2S6QJKFNzX70bC1ztbuoahvUEd3tonO8zPH-apuBtn0ZqEk0zS_yEx_zBw6DHYh3DZkRVjE-3MPmbC-xgghnCFvfzE_oHoGicMMzt-NWBV6KDZ0Lme1s3HVPhGGsEf268m5DOqVNcA8v7biugpTaS_8IwNNaJ8zA0oVIVPzkwTNOScIe58N2Fid38OSbp98gda4HEmrU499idIuKwIqtLwVl9Eoz_-WPBckQJN8XWuPWUTiQQwqIwckOHQ"}

    #auth_header = {"Authorization": "Bearer "}

    host = "http://localhost:8082"

    wine_id = 1
    # create wine
    @task(11)
    def create_wine(self):
        self.client.post("/wine", json={
            "variety": "Pálava",
            "color": "red",
            "year": 2023,
            "sweetness": 4,
            "alcoholContent": 13,
            "description": "World Class",
            "price": 200
        }, headers=self.auth_header)

    # get wine
    @task(12)
    def get_wine(self):
        self.client.get("/wine/{}".format(self.wine_id), headers=self.auth_header)

    # edit wine
    @task(13)
    def edit_wine(self):
        self.client.patch("/wine/{}".format(self.wine_id), json={
            "variety": "Pálava",
            "color": "red",
            "year": 2023,
            "sweetness": 4,
            "alcoholContent": 13,
            "description": "The best wine.",
            "price": 240
        }, headers=self.auth_header)

    # get all wines
    @task(14)
    def get_all_wines(self):
        self.client.get("/wines", headers=self.auth_header)

    # edit stock wine information
    @task(15)
    def edit_stock_wine(self):
        self.client.patch("/stockWine", json={
            "id": 1,
            "bottlesSold": 0,
            "bottlesAvailable": 180,
            "price": 260,
            "profit": "42"
        }, headers=self.auth_header)

    # get stock wine information
    @task(16)
    def get_stock_wine(self):
        self.client.get("/stockWine/{}".format(self.wine_id), headers=self.auth_header)

    # get all stock wines
    @task(17)
    def get_all_stock_wines(self):
        self.client.get("/stockWines", headers=self.auth_header)


    # get all orders
    @task(19)
    def get_all_orders(self):
        self.client.get("/orders", headers=self.auth_header)

class RegularUser(HttpUser):
    host = "http://localhost:8082"
    wait_time = between(2, 5)
    auth_header = {"Authorization": "Bearer eyJraWQiOiJyc2ExIiwidHlwIjoiYXQrand0IiwiYWxnIjoiUlMyNTYifQ.eyJhdWQiOiI3ZTAyYTBhOS00NDZhLTQxMmQtYWQyYi05MGFkZDQ3YjBmZGQiLCJzdWIiOiI0ODU0NzJAbXVuaS5jeiIsImFjciI6Imh0dHBzOi8vcmVmZWRzLm9yZy9wcm9maWxlL3NmYSIsInNjb3BlIjoidGVzdF80IHRlc3RfMyB0ZXN0XzIgdGVzdF8xIHRlc3RfcmVhZCBvcGVuaWQgcHJvZmlsZSBlZHVwZXJzb25fc2NvcGVkX2FmZmlsaWF0aW9uIHRlc3Rfd3JpdGUgZW1haWwgdGVzdF81IiwiYXV0aF90aW1lIjoxNjgzOTg3MzAyLCJpc3MiOiJodHRwczovL29pZGMubXVuaS5jei9vaWRjLyIsImV4cCI6MTY4NDA2OTA0MCwiaWF0IjoxNjg0MDY1NDQwLCJjbGllbnRfaWQiOiI3ZTAyYTBhOS00NDZhLTQxMmQtYWQyYi05MGFkZDQ3YjBmZGQiLCJqdGkiOiI5NDk4MjEzMy1jMDZlLTRlYjgtYmZkYS04Mzg2MDkwNDNmYjAifQ.xLGpwHXu-mj7AMLsltmqL8QyK9RjvF1C1PoDueAjyMDHQH-nIu8TNYUf2eeR8-yr03jDDTw4kQ-DTVxmaYGjZ4pJCkED2S6QJKFNzX70bC1ztbuoahvUEd3tonO8zPH-apuBtn0ZqEk0zS_yEx_zBw6DHYh3DZkRVjE-3MPmbC-xgghnCFvfzE_oHoGicMMzt-NWBV6KDZ0Lme1s3HVPhGGsEf268m5DOqVNcA8v7biugpTaS_8IwNNaJ8zA0oVIVPzkwTNOScIe58N2Fid38OSbp98gda4HEmrU499idIuKwIqtLwVl9Eoz_-WPBckQJN8XWuPWUTiQQwqIwckOHQ"}
    #auth_header = {"Authorization": "Bearer "}
    #auth_header = {"Authorization": "Bearer "}

    order_id = 1

    # buy wine
    @task(1)
    def buy_wine(self):
        self.client.post("/order",json={
            "userId": "3451",
            "deliveryAddress": {
                "street": "Ulice 123",
                "city": "Brno",
                "zipCode": "12345",
                "country": "Czech Republic"
            },
            "telephoneNumber": "+420 123 456 789",
            "orderItems": [
                {
                    "id": 1,
                    "amount": 2
                }
            ],
        }, headers=self.auth_header)

    # get order
    @task(2)
    def get_order(self):
        self.client.get("/order/{}".format(self.order_id), headers=self.auth_header)

    # get all orders
    @task(3)
    def get_all_orders(self):
        self.client.get("/orders", headers=self.auth_header)

    #get all wines
    @task(4)
    def get_all_wines(self):
        self.client.get("/wines", headers=self.auth_header)
